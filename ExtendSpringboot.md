13. Extending Spring Boot
Felipe Gutierrez1 
(1)
Albuquerque, NM, USA
 

Developers and software architects are often looking for design patterns to apply, new algorithms to implement, reusable components that are easy to use and maintain, and new ways to improve development. It’s not always easy to find a unique or perfect solution, and it’s necessary to use different technologies and methodologies to accomplish the goal of having an application that runs and never fails.

This chapter explains how the Spring and Spring Boot teams created a pattern for reusable components that are easy to use and implement. Actually, you have been learning about this pattern throughout the entire book, especially in the Spring Boot configuration chapter.

This chapter covers auto-configuration in detail, including how you can extend and create new Spring Boot modules that are reusable. Let’s get started.
Creating a spring-boot-starter

In this section, I show you how to create a custom spring-boot-starter, but let’s discuss some of the requirements first. Because you are working in the ToDo app, this custom starter is a client that you can use to do any operations for the ToDo’s, such as create, find, and findAll. This client needs a host that connects to a ToDo REST API service.
Let’s start by setting up the project. So far, there is no template that sets up a baseline for a custom spring-boot-starter, so, we need to do this manually. Create the following structure.
todo-client-starter/
├── todo-client-spring-boot-autoconfigure
└── todo-client-spring-boot-starter

You need to create a folder named todo-client-starter, where you create two subfolders: todo-client-spring-boot-autoconfigure and todo-client-spring-boot-starter. Yes, there is a naming convention here. The Spring Boot team suggests that any custom starter follow this naming convention: <name-of-starter>-spring-boot-starter and <name-of-starter>-spring-boot-autoconfigure. The autoconfigure module has all the code and necessary dependencies that the starter needs; don’t worry, I give you the information on what is needed.
First, let’s create a main pom.xml file that has two modules: autoconfigure and starter. Create a pom.xml file in the todo-client-starter folder . Your structure should look like this:
todo-client-starter/
├── pom.xml
├── todo-client-spring-boot-autoconfigure
└── todo-client-spring-boot-starter
The pom.xml file looks Listing 13-1.
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>
      <groupId>com.apress.todo</groupId>
      <artifactId>todo-client</artifactId>
      <version>0.0.1-SNAPSHOT</version>
      <packaging>pom</packaging>
      <name>todo-client</name>
      <modules>
            <module>todo-client-spring-boot-autoconfigure</module>
            <module>todo-client-spring-boot-starter</module>
      </modules>
      <dependencyManagement>
            <dependencies>
                  <dependency>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-dependencies</artifactId>
                        <version>2.0.5.RELEASE</version>
                        <type>pom</type>
                        <scope>import</scope>
                  </dependency>
            </dependencies>
      </dependencyManagement>
</project>
Listing 13-1

todo-client-starter/pom.xml

Listing 13-1 shows the main pom.xml that has two modules. One important thing to mention is that the <packaging/> tag is a pom, because at the end it is necessary to install these jars into the local repo to be used later. Also important is that this pom is declaring a <dependencyManagement/> tag that allows us to use the Spring Boot jars and all its dependencies. At the end, we don’t need to declare versions.
todo-client-spring-boot-starter
Next, let’s create another pom.xml file in the todo-client-spring-boot-starter folder. You should have the following structure.
todo-client-starter/
├── pom.xml
├── todo-client-spring-boot-autoconfigure
└── todo-client-spring-boot-starter
    └── pom.xml
See Listing 13-2.
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.apress.todo</groupId>
    <artifactId>todo-client-spring-boot-starter</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>todo-client-spring-boot-starter</name>
    <description>Todo Client Spring Boot Starter</description>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>
 <parent>
        <groupId>com.apress.todo</groupId>
        <artifactId>todo-client</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>..</relativePath>
    </parent>
 <dependencies>
        <dependency>
            <groupId>com.apress.todo</groupId>
            <artifactId>todo-client-spring-boot-autoconfigure</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
    </dependencies>
</project>
Listing 13-2

todo-client-starter/todo-client-spring-boot-starter/pom.xml

As you can see, Listing 13-2 is nothing new. It is declaring a <parent/> tag that relates to the previous pom.xml file, and it is declaring the autoconfigure module.

That’s it for the todo-client-spring-boot-starter; nothing else. You can see this as a marker where you declare the modules that do the heavy work.
todo-client-spring-boot-autoconfigure
Next, let’s create the structure for within the todo-client-spring-boot-autoconfigure folder. You should have the following final structure.
todo-client-starter/
├── pom.xml
├── todo-client-spring-boot-autoconfigure
│   ├── pom.xml
│   └── src
│       └── main
│           ├── java
│           └── resources
└── todo-client-spring-boot-starter
    └── pom.xml
Your todo-client-spring-boot-autoconfigure folder should look like this:
todo-client-spring-boot-autoconfigure/
├── pom.xml
└── src
    └── main
        ├── java
        └── resources
A basic Java project structure. Let’s start with the pom.xml file (see Listing 13-3).
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.apress.todo</groupId>
    <artifactId>todo-client-spring-boot-autoconfigure</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <packaging>jar</packaging>
    <name>todo-client-spring-boot-autoconfigure</name>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
    </properties>
    <parent>
        <groupId>com.apress.todo</groupId>
        <artifactId>todo-client</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>..</relativePath>
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.2</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.hateoas</groupId>
            <artifactId>spring-hateoas</artifactId>
        </dependency>
        <!-- JSON / Traverson -->
        <dependency>
            <groupId>org.springframework.plugin</groupId>
            <artifactId>spring-plugin-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
Listing 13-3

todo-client-starter/todo-client-spring-boot-autoconfigure

In this case, the autoconfigure project depends on the web, Lombok, security, Hateoas , and JsonPath.
spring.factories

If you remember from the first chapters, I told you about the Spring Boot way to auto-configure everything based on the classpath; that’s the real magic behind Spring Boot. I mentioned that when the application starts, the Spring Boot auto-configuration loads all the classes from the META-INF/spring.factories files to do each auto-configuration class, which provides the app the defaults it needs to run. Remember, Spring Boot is an opinionated runtime for Spring applications.
Let’s create the spring.factories file that contains the class that does the auto-configuration and sets some defaults (see Listing 13-4).
org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.apress.todo.configuration.ToDoClientAutoConfiguration
Listing 13-4

src/main/resources/META-INF/spring.factories

Note that the spring.factories file is declaring the ToDoClientAutoConfiguration class.
auto-configuration
Let’s start by creating the ToDoClientAutoConfiguration class (see Listing 13-5).
package com.apress.todo.configuration;
import com.apress.todo.client.ToDoClient;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.Resource;
import org.springframework.web.client.RestTemplate;
@RequiredArgsConstructor
@Configuration
@ConditionalOnClass({Resource.class, RestTemplateBuilder.class})
@EnableConfigurationProperties(ToDoClientProperties.class)
public class ToDoClientAutoConfiguration {
    private final Logger log = LoggerFactory.getLogger(ToDoClientAutoConfiguration.class);
    private final ToDoClientProperties toDoClientProperties;
    @Bean
    public ToDoClient client(){
        log.info(">>> Creating a ToDo Client...");
        return new ToDoClient(new RestTemplate(),this.toDoClientProperties);
    }
}
Listing 13-5

com.apress.todo.configuration.ToDoClientAutoConfiguration.java

Listing 13-5 shows the auto-configuration that is executed. It is using the @ConditionalOnClass annotation, which says that if it finds in the classpath, Resource.class and RestTemplateBuilder.class will continue with the configuration. Of course, because one of the dependencies is spring-boot-starter-web, it has those classes. But what happens when somebody excludes these resources? That’s when this class does its job.

This class is declaring a TodoClient bean that uses a RestTemplate and the ToDoClientProperties instances.

That’s it. Very simple auto-configuration. It sets the default ToDoClient bean if it finds those resource classes in your project where you are using this custom starter.
Helper Classes
Next, let’s create the helper classes. Create the ToDoClientProperties and the ToDoClient classes (see Listings 13-6 and 13-7).
package com.apress.todo.configuration;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix="todo.client")
public class ToDoClientProperties {
    private String host = "http://localhost:8080";
    private String path = "/toDos";
}
Listing 13-6

com.apress.todo.configuration.ToDoClientProperties.java
As you can see, nothing new—just two fields that hold a default for the host and the path. This means that you can override them in the application.properties files .
package com.apress.todo.client;
import com.apress.todo.configuration.ToDoClientProperties;
import com.apress.todo.domain.ToDo;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.URI;
import java.util.Collection;
@AllArgsConstructor
@Data
public class ToDoClient {
    private RestTemplate restTemplate;
    private ToDoClientProperties props;
    public ToDo add(ToDo toDo){
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .uri(URI.create(this.props.getHost())).path(this.props.getPath()).build();
        ResponseEntity<ToDo> response =
                this.restTemplate.exchange(
                        RequestEntity.post(uriComponents.toUri())
                                .body(toDo)
                        ,new ParameterizedTypeReference<ToDo>() {});
        return response.getBody();
    }
    public ToDo findById(String id){
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .uri(URI.create(this.props.getHost())).pathSegment(this.props.getPath(), "/{id}")
                .buildAndExpand(id);
        ResponseEntity<ToDo> response =
                this.restTemplate.exchange(
                        RequestEntity.get(uriComponents.toUri()).accept(MediaTypes.HAL_JSON).build()
                        ,new ParameterizedTypeReference<ToDo>() {});
        return response.getBody();
    }
    public Collection<ToDo> findAll() {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .uri(URI.create(this.props.getHost())).build();
        Traverson traverson = new Traverson(uriComponents.toUri(), MediaTypes.HAL_JSON, MediaType.APPLICATION_JSON_UTF8);
        Traverson.TraversalBuilder tb = traverson.follow(this.props.getPath().substring(1));
        ParameterizedTypeReference<Resources<ToDo>> typeRefDevices = new ParameterizedTypeReference<Resources<ToDo>>() {};
        Resources<ToDo> toDos = tb.toObject(typeRefDevices);
        return toDos.getContent();
    }
}
Listing 13-7

com.apress.todo.client.ToDoClient.java

The ToDoClient class is a very straightforward implementation. This class is using the RestTemplate in all the methods; even though the findAll method is using a Traverson (an implementation in Java of the JavaScript Traverson library ( https://github.com/traverson/traverson ), which is a way to manipulate all the HATEOAS links) instance; behind the scenes it is using the RestTemplate.

Take a few minutes to analyze the code. Remember that this is a client that is requesting and posting to a ToDo’s REST API server.
To use this client, it is necessary to create the ToDo domain class (see Listing 13-8).
package com.apress.todo.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
public class ToDo {
    private String id;
    private String description;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modified;
    private boolean completed;
    public ToDo(String description){
        this.description = description;
    }
}
Listing 13-8

com.apress.todo.domain.ToDo.java

Here we are introducing @Json* annotations. One ignores any of the links (provided by the HAL+JSON protocol) and one serializes the LocalDateTime instances.

We are almost done. Let’s add a security utility that helps encrypt/decrypt ToDo descriptions.
Creating an @Enable* Feature

One of the cool features of the Spring and Spring Boot technologies is that they expose several @Enable* features that hide all the boilerplate configuring and does the heavy lifting for us.
So, let’s create a custom @EnableToDoSecurity feature . Let’s start by creating the annotation that is picked up by Spring Boot, auto-configuration (see Listing 13-9).
package com.apress.todo.annotation;
import com.apress.todo.security.ToDoSecurityConfiguration;
import org.springframework.context.annotation.Import;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(ToDoSecurityConfiguration.class)
public @interface EnableToDoSecurity {
    Algorithm algorithm() default Algorithm.BCRYPT;
}
Listing 13-9

com.apress.todo.annotation.EnableToDoSecurity.java
This annotation uses an algorithm enum; let’s create it (see Listing 13-10).
package com.apress.todo.annotation;
public enum Algorithm {
    BCRYPT, PBKDF2
}
Listing 13-10

com.apress.todo.annotation.Algorithm.java

This means that we can have some parameters passed to the @EnableToDoSecurity. We choose either BCRYPT or PBKDF2, and if there is no parameter, by default it is BCRYPT.
Next, create the ToDoSecurityConfiguration class that triggers any configuration if the @EnableToDoSecurity is declared (see Listing 13-11).
package com.apress.todo.security;
import com.apress.todo.annotation.Algorithm;
import com.apress.todo.annotation.EnableToDoSecurity;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
public class ToDoSecurityConfiguration implements ImportSelector {
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        AnnotationAttributes attributes =
                AnnotationAttributes.fromMap(
                        annotationMetadata.getAnnotationAttributes(EnableToDoSecurity.class.getName(), false));
        Algorithm algorithm = attributes.getEnum("algorithm");
        switch(algorithm){
            case PBKDF2:
                return new String[] {"com.apress.todo.security.Pbkdf2Encoder"};
            case BCRYPT:
            default:
                return new String[] {"com.apress.todo.security.BCryptEncoder"};
        }
    }
}
Listing 13-11

com.apress.todo.security.ToDoSecurityConfiguration.java

Listing 13-11 shows you that the auto-configuration is executed only if the @EnableToDoSecurity annotation is declared. Spring Boot also tracks every class that implements the ImportSelector interface, which hides all the boilerplate processing annotations.

So, if the @EnableToDoSecurity annotation is found, then this class is executed by calling the selectImports method that returns an array of Strings that are classes that have to be configured; in this case, either the com.apress.todo.security.Pbkdf2Encoder class (if you set the PBKDF2 algorithm as parameter) or the com.apress.todo.security.BCryptEncoder class (if you set the BCRYPT algorithm as the parameter).
What is special about these classes? Let’s create the BCryptEncoder and Pbkdf2Encoder classes (see Listing 13-12 and Listing 13-13).
package com.apress.todo.security;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@Configuration
public class BCryptEncoder {
    @Bean
    public ToDoSecurity utils(){
        return new ToDoSecurity(new BCryptPasswordEncoder(16));
    }
}
Listing 13-12

com.apress.todo.security.BCryptEncoder.java
package com.apress.todo.security;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
@Configuration
public class Pbkdf2Encoder {
    @Bean
    public ToDoSecurity utils(){
        return new ToDoSecurity(new Pbkdf2PasswordEncoder());
    }
}
Listing 13-13

com.apress.todo.security.Pbkdf2Encoder.java

Both classes are declaring the ToDoSecurity bean. So, if you choose the PBKDF2 algorithm, then the ToDoSecurity bean is created with the Pbkdf2PasswordEncoder instance; and if you choose the BCRYPT algorithm, the ToDoSecurity bean is created with the BCryptPasswordEncoder(16) instance.
Listing 13-14 shows the ToDoSecurity class .
package com.apress.todo.security;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
@AllArgsConstructor
@Data
public class ToDoSecurity {
    private PasswordEncoder encoder;
}
Listing 13-14

com.apress.todo.security.ToDoSecurity.java

As you can see, nothing special in this class.
ToDo REST API Service
Let’s prepare the ToDo REST API service. You can reuse the todo-rest project that uses data-jpa and data-rest , which you did in other chapters. Let’s review it and see what we need to do (see Listings 13-15, 13-16, and 13-17).
package com.apress.todo.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Entity
@Data
@NoArgsConstructor
public class ToDo {
    @NotNull
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @NotNull
    @NotBlank
    private String description;
    @Column(insertable = true, updatable = false)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modified;
    private boolean completed;
    public ToDo(String description){
        this.description = description;
    }
    @PrePersist
    void onCreate() {
        this.setCreated(LocalDateTime.now());
        this.setModified(LocalDateTime.now());
    }
    @PreUpdate
    void onUpdate() {
        this.setModified(LocalDateTime.now());
    }
}
Listing 13-15

com.apress.todo.domain.ToDo.java
This ToDo class is nothing new; you already know about every annotation used here. The only difference is that it is using the @Json* annotations only for the dates with a specific format.
package com.apress.todo.repository;
import com.apress.todo.domain.ToDo;
import org.springframework.data.repository.CrudRepository;
public interface ToDoRepository extends CrudRepository<ToDo,String> {
}
Listing 13-16

com.apress.todo.repository.ToDoRepository.java
The same as before; nothing that you don’t know about this interface.
package com.apress.todo.config;
import com.apress.todo.domain.ToDo;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
@Configuration
public class ToDoRestConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(ToDo.class);
    }
}
Listing 13-17

com.apress.todo.config.ToDoRestConfig.java

Listing 13-17 shows you a new class, the ToDoRestConfig that is extending from the RespositoryRestConfigurerAdapter; this class can help configure part of the RestController implementation from everything that is by default configured by the JPA repositories auto-configuration. It overrides the configureRepositoryRestConfiguration by exposing the IDs of the domain class. When we worked with REST in other chapters, the IDs never showed upon request; but with this overriding, we can make it happen. And we need this feature because we want to get back the ID of the ToDo instance.
In application.properties, you should have the following.
# JPA
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true

Again, nothing new.
Installing and Testing
Let’s prepare everything to run on a new custom starter. Let’s start by installing the todo-client-spring-boot-starter. Open a terminal and go to your todo-client-starter folder and execute the following command.
$ mvn clean install

This command installs your jar in the local .m2 directory, which is ready to be picked up by another project that uses it.
Task Project
Now that you installed todo-client-spring-boot-starter, it is time to test it. You are going to create a new project. You can create the project as usual. Go to the Spring Initializr ( https://start.spring.io ) and set the fields with the following values.

    Group: com.apress.task

    Artifact: task

    Name: task

    Package Name: com.apress.task

You can select either Maven or Gradle. Then click the Generate Project button. This generates and downloads a ZIP file. You can uncompress it, and then import it into your favorite IDE (see Figure 13-1).
../images/340891_2_En_13_Chapter/340891_2_En_13_Fig1_HTML.jpg
Figure 13-1

Spring Initializr
Next, you need to add todo-client-spring-boot-starter. If you are using Maven, go to your pom.xml file and add the dependency.
        <dependency>
            <groupId>com.apress.todo</groupId>
            <artifactId>todo-client-spring-boot-starter</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
If you are using Gradle, add the dependency to your build.gradle file.
compile('com.apress.todo:todo-client-spring-boot-starter:0.0.1-SNAPSHOT')
That’s it. Now open the main class, in which there is the code shown in Listing 13-18.
package com.apress.task;
import com.apress.todo.annotation.EnableToDoSecurity;
import com.apress.todo.client.ToDoClient;
import com.apress.todo.domain.ToDo;
import com.apress.todo.security.ToDoSecurity;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
@EnableToDoSecurity
@SpringBootApplication
public class TaskApplication {
      public static void main(String[] args) {
        SpringApplication app = new SpringApplication(TaskApplication.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        app.run(args);
      }
      @Bean
    ApplicationRunner createToDos(ToDoClient client){
          return args -> {
            ToDo toDo = client.add(new ToDo("Read a Book"));
            ToDo review = client.findById(toDo.getId());
            System.out.println(review);
            System.out.println(client.findAll());
        };
    }
    @Bean
    ApplicationRunner secure(ToDoSecurity utils){
        return args -> {
            String text = "This text will be encrypted";
            String hash = utils.getEncoder().encode(text);
            System.out.println(">>> ENCRYPT: " + hash);
            System.out.println(">>> Verify: " + utils.getEncoder().matches(text,hash));
        };
    }
}
Listing 13-18

com.apress.task.TaskApplication.java

There are two ApplicationRunner beans; each has a parameter. createToDos uses the ToDoClient bean instance (if you don’t have the RestTemplateBuilder or Resource, it will fail). It is using the methods you know (add, findById and findAll).

The method secure is using the ToDoSecurity bean instance, which is possible thanks to @EnableToDoSecurity. If you remove it or comment it out, it tells you it can’t find the ToDoSecurity bean.

Take a few minutes to analyze the code to see what’s going on.
Running the Task App

To run the app, first make sure the todo-rest app is up and running. It should run on port 8080. Remember that you have already installed the todo-client-spring-boot-starter with the mvn clean install command.

So if you are running it, you see some responses, and the ToDo is saved in the ToDo REST service. It also shows you the encrypted text.
If you are running the ToDo REST API in a different IP, host, port, or path, you can change the defaults by using the todo.client.* properties in the application.properties file.
# ToDo Rest API
todo.client.host=http://some-new-server.com:9091
todo.client.path=/api/toDos
Remember that if you don’t override, it defaults to http://localhost:8080 and /toDos. After running the Task app, you should see something similar to the following output.
INFO - [ main] c.a.t.c.ToDoClientAutoConfiguration      : >>> Creating a ToDo Client...
INFO - [ main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
INFO - [ main] com.apress.task.TaskApplication          : Started TaskApplication in 1.047 seconds (JVM running for 1.853)
ToDo(id=8a8080a365f427c00165f42adee50000, description=Read a Book, created=2018-09-19T17:29:34, modified=2018-09-19T17:29:34, completed=false)
[ToDo(id=8a8080a365f427c00165f42adee50000, description=Read a Book, created=2018-09-19T17:29:34, modified=2018-09-19T17:29:34, completed=false)]
>>> ENCRYPT: $2a$16$pVOI../twnLwN3GFiChdR.zRFfyCIZMEbwEXbAtRoIHqxeLB3gmUG
>>> Verify: true

Congratulations! You just have created your first custom Spring Boot starter and @Enable feature!
Note

Remember that you can get the book source code from the Apress website or on GitHub at https://github.com/Apress/pro-spring-boot-2 .
Summary

This chapter showed you how to create a module for Spring Boot by using the auto-configuration pattern. I showed you how to create your custom health monitor. As you can see, it’s very simple to extend Spring Boot apps, so feel free to modify the code and experiment with them.

We didn’t do much if any unit or integration testing. It would be good homework for you to practice all the details that I showed you. I think it will help you understand how Spring Boot works even better. Repeat and you will master!