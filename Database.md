Data has become the most important part of the IT world, from trying to access, persist, and analyze it, to using a few bytes to petabytes of information. There have been many attempts to create frameworks and libraries to facilitate a way for developers to interact with the data, but sometimes this becomes too complicated.

After version 3.0, the Spring Framework created different teams that specialized in the different technologies. The Spring Data project team was born. This particular project’s goal is to simplify uses of data access technologies, from relational and non-relational databases, to map-reduce frameworks and cloud-based data services. This Spring Data project is a collection of subprojects specific to a given database.

This chapter covers data access with Spring Boot using the ToDo application from previous chapters. You are going to make the ToDo app work with SQL and NoSQL databases. Let’s get started.

SQL Databases

Do you remember those days when (in the Java world) you needed to deal with all the JDBC (Java Database Connectivity) tasks? You had to download the correct drivers and connection strings, open and close connections, SQL statements, result sets, and transactions, and convert from result sets to objects. In my opinion, these are all very manual tasks. Then a lot of ORM (object-relational mapping) frameworks started to emerge to manage these tasks—frameworks like Castor XML, ObjectStore, and Hibernate, to mention a few. They allowed you to identify the domain classes and create XML that was related to the database’s tables. At some point, you also needed to be an expert to manage those kinds of frameworks.

The Spring Framework helped a lot with those frameworks by following the template design pattern . It allowed you create an abstract class that defined ways to execute the methods and created the database abstractions that allowed you to focus only on your business logic. It left all the hard lifting to the Spring Framework, including handling connections (open, close, and pooling), transactions, and the way you interact with the frameworks.

It’s worth mentioning that the Spring Framework relies on several interfaces and classes (like the javax.sql.DataSource interface) to get information about the database you are going to use, how to connect to it (by providing a connection string), and its credentials. Now, if you have transaction management to do, the DataSource interface is essential. Normally, the DataSource interface requires the Driver class, the JDBC URL, a username, and a password to connect to the database.

Spring Data

The Spring Data team has created some of the amazing data-driven frameworks available today for the Java and Spring community. Their mission is to provide familiar and consistent Spring-based programming for data access and total control of the underlying data store technology that you want to use.

The Spring Data project is the umbrella for several additional libraries and data frameworks, which makes it easy to use data access technologies for relational and non-relation databases (a.k.a. NoSQL).
The following are some of the Spring Data features.

    Support for cross-store persistence

    Repository-based and custom object-mapping abstractions

    Dynamic queries based on method names

    Easy Spring integration via JavaConfig and XML

    Support for Spring MVC controllers

    Events for transparent auditing (created, last changes)

There are plenty more features—an entire book would be needed to cover each one of them. This chapter covers just enough to create powerful data-driven applications. Remember that Spring Data is the main umbrella project for everything that is covered.

Spring JDBC

In this section, I show you how to use the JdbcTemplate class . This particular class implements the template design pattern, which is a concrete class that exposes defined ways or templates to execute its methods. It hides all the boilerplate algorithms or a set of instructions. In Spring, you can choose different ways to form the basis for your JDBC database access; using the JdbcTemplate class is the classic Spring JDBC approach and this is the lowest level.

When you are using the JdbcTemplate class, you only need to implement callback interfaces to create an easy way to interact with any database engine. The JdbcTemplate class requires a javax.sql.DataSource and can be used in any class, by declaring it in JavaConfig, XML, or by annotations. The JdbcTemplate class takes care of all SQLExceptions and are properly handled.

You can use NamedParameterJdbcTemplate (a JdbcTemplate wrapper) to provide named parameters (:parameterName), instead of the traditional JDBC "?" placeholders. This is another option for your SQL queries.
The JdbcTemplate class exposes different methods.

    Querying (SELECT). You normally use the query, queryForObject method calls.

    Updating (INSERT/UPDATE/DELETE). You use the update method call.

    Operations (database/table/functions). You use the execute and update method calls.

With Spring JDBC, you have the ability to call stored procedures by using the SimpleJdbcCall class and manipulating the result with a particular RowMapper interface . RowMapper<T> is used by the JdbcTemplate class for mapping rows of ResultSet on a per-row basis.

Spring JDBC has support for embedded database engines, such as HSQL, H2 and Derby. It is easy to configure and offers quick startup time and testability.

Another feature is the ability to initialize the database with scripts; you can use embedded support or not. You can add your own schemas and data in a SQL format.
JDBC with Spring Boot

The Spring Framework has the support for working with SQL databases either using JDBC or ORMs (I cover this in the following sections). Spring Boot brings even more to data applications.

Spring Boot uses auto-configuration to set sensible defaults when it finds out that your application has a JDBC JARs. Spring Boot auto-configures the datasource based on the SQL driver in your classpath. If it finds that you have any of the embedded database engines (H2, HSQL or Derby), it is configured by default; in other words, you can have two driver dependencies (e.g., MySQL and H2), and if Spring Boot doesn’t find any declared datasource bean, it creates it based on the embedded database engine JAR in your classpath (e.g., H2). Spring Boot also configures HikariCP as connection pool manager by default. Of course, you can override these defaults.
If you want to override the defaults, then you need to provide your own datasource declaration, either JavaConfig, XML, or in the application.properties file.
# Custom DataSource
spring.datasource.username=springboot
spring.datasource.password=rocks!
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false

src/main/resources/application.properties
Spring Boot supports JNDI connections if you are deploying your app in an application container. You can set the JNDI name in the application.properties file.
spring.datasource.jndi-name=java:jboss/ds/todos

Another feature that Spring Boot brings to data apps is that if you have a file named schema.sql, data.sql, schema-<platform>.sql, or data-<platform>.sql in the classpath, it initializes your database by executing those script files.

So, If you want to use JDBC in your Spring Boot application, you need to add the spring-boot-starter-jdbc dependency and your SQL driver.
ToDo App with JDBC
Is time to work with the ToDo app as we did in the previous chapter. You can start from scratch or you can follow along. If you are starting from scratch, then you can go to Spring Initializr ( https://start.spring.io ) and add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-jdbc

    Name: todo-jdbc

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, JDBC, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 5-1).

You can copy all the classes from the previous chapter, except for the ToDoRepository class; this is the only class that is new. Also make sure that in the pom.xml or build.gradle files, there are two drivers: H2 and MySQL. Based on what I discussed in the previous section, if I don’t specify any datasource (in the JavaConfig, XML or application.properties) what does Spring Boot auto-configuration do? Correct! Spring Boot auto-configures the H2 embedded database by default.
Repository: ToDoRepository
Create a ToDoRepository class that implements the CommonRepository interface (see Listing 5-1).
package com.apress.todo.repository;
import com.apress.todo.domain.ToDo;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.*;
@Repository
public class ToDoRepository implements CommonRepository<ToDo> {
    private static final String SQL_INSERT = "insert into todo (id, description, created, modified, completed) values (:id,:description,:created,:modified,:completed)";
    private static final String SQL_QUERY_FIND_ALL = "select id, description, created, modified, completed from todo";
    private static final String SQL_QUERY_FIND_BY_ID = SQL_QUERY_FIND_ALL + " where id = :id";
    private static final String SQL_UPDATE = "update todo set description = :description, modified = :modified, completed = :completed where id = :id";
    private static final String SQL_DELETE = "delete from todo where id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    public ToDoRepository(NamedParameterJdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    private RowMapper<ToDo> toDoRowMapper = (ResultSet rs, int rowNum) -> {
        ToDo toDo = new ToDo();
        toDo.setId(rs.getString("id"));
        toDo.setDescription(rs.getString("description"));
        toDo.setModified(rs.getTimestamp("modified").toLocalDateTime());
        toDo.setCreated(rs.getTimestamp("created").toLocalDateTime());
        toDo.setCompleted(rs.getBoolean("completed"));
        return toDo;
    };
    @Override
    public ToDo save(final ToDo domain) {
        ToDo result = findById(domain.getId());
        if(result != null){
            result.setDescription(domain.getDescription());
            result.setCompleted(domain.isCompleted());
            result.setModified(LocalDateTime.now());
            return upsert(result, SQL_UPDATE);
        }
        return upsert(domain,SQL_INSERT);
    }
    private ToDo upsert(final ToDo toDo, final String sql){
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("id",toDo.getId());
        namedParameters.put("description",toDo.getDescription());
        namedParameters.put("created",java.sql.Timestamp.valueOf(toDo.getCreated()));
        namedParameters.put("modified",java.sql.Timestamp.valueOf(toDo.getModified()));
        namedParameters.put("completed",toDo.isCompleted());
        this.jdbcTemplate.update(sql,namedParameters);
        return findById(toDo.getId());
    }
    @Override
    public Iterable<ToDo> save(Collection<ToDo> domains) {
        domains.forEach( this::save);
        return findAll();
    }
    @Override
    public void delete(final ToDo domain) {
        Map<String, String> namedParameters = Collections.singletonMap("id", domain.getId());
        this.jdbcTemplate.update(SQL_DELETE,namedParameters);
    }
    @Override
    public ToDo findById(String id) {
        try {
            Map<String, String> namedParameters = Collections.singletonMap("id", id);
            return this.jdbcTemplate.queryForObject(SQL_QUERY_FIND_BY_ID, namedParameters, toDoRowMapper);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    @Override
    public Iterable<ToDo> findAll() {
        return this.jdbcTemplate.query(SQL_QUERY_FIND_ALL, toDoRowMapper);
    }
}
Listing 5-1

com.apress.todo.respository.ToDoRepository.java

Listing 5-1 shows the ToDoRepository class that uses the JdbcTemplate, not directly tough. This class is using NamedParameterJdbcTemplate (a JdbcTemplate wrapper) that helps with all the named parameters, which means that instead of using ? in your SQL statements, you use names like :id.

This class is also declaring a RowMapper; remember that the JdbcTemplate used the RowMapper for mapping rows of a ResultSet on a per-row basis.

Analyze the code and check out every method implementation that is using plain SQL in every method.
Database Initialization: schema.sql
Remember that the Spring Framework allows you to initialize your database—creating or altering any table, or inserting/updating data when your application starts. To initialize a Spring app (not Spring Boot), it is necessary to add configuration (XML or JavaConfig); but the ToDo app is Spring Boot. If Spring Boot finds the schema.sql and/or data.sql files , it executes them automatically. Let’s create the schema.sql (see Listing 5-2).
DROP TABLE IF EXISTS todo;
CREATE TABLE todo
(
  id varchar(36) not null primary key,
  description varchar(255) not null,
  created timestamp,
  modified timestamp,
  completed boolean
);
Listing 5-2

src/main/resources/schema.sql

Listing 5-2 shows the schema.sql that executes when the application is starting, and because H2 is the default datasource that is configured, then this script is executed against the H2 engine.
Running and Testing: ToDo App
Now it’s time to run and test the ToDo app. You can run it within your IDE, or if you are using Maven, execute
./mvnw spring-boot:run
If you are using Gradle, execute
./gradlew bootRun

To test the ToDo app, you can run your ToDoClient app. It should work without any problems.
H2 Console

Now that you ran the ToDo app, how can you make sure that the app is saving the data in the H2 engine? Spring Boot has a property that enables a H2 console so that you can interact with it. It is very useful for development purposes but not for production environments.
Modify the application.properties file and add the following property.
spring.h2.console.enabled=true

src/main/resources/application.properties
Restart the ToDo app, add values with a cURL command, and go to your browser and hit http://localhost:8080/h2-console. (see Figure 5-2).

Figure 5-2 shows the H2 console, which you can reach at the /h2-console endpoint. (you can override this endpoint as well). The JDBC URL must be jdbc:h2:mem:testdb (sometimes this is different, so change it to that value). By default, the database name is testdb (but you can override this as well). If you click the Connect button, you get a different view, in which you can see the table and data being created (see Figure 5-3).

Figure 5-3 shows that you can execute any SQL query and get data back. If you want to see which SQL queries are being executed in the ToDo app, you can add the following properties to the application.properties file.
logging.level.org.springframework.data=INFO
logging.level.org.springframework.jdbc.core.JdbcTemplate=DEBUG

src/main/resources/application.properties

As you can see, the JdbcTemplate class offers you a lot of possibilities to interact with any database engine, but this class is the “lowest level” approach.

At the time of this writing, there was a new way to use the JdbcTemplate class in a more uniform way—the Spring Data way (something that I describe in the following sections). The Spring Data team has created the new Spring Data JDBC project , which follows the aggregate root concept, as described in the book Domain-Driven Design by Eric Evans (Addison-Wesley Professional, 2003). It has many features, such as CRUD operations, support for @Query annotations, support for MyBatis queries, events, and more, so keep an eye on this project. It is a new way to do JDBC.
Spring Data JPA

The JPA (Java Persistence API) provides a POJO persistence model for object-relational mapping. Spring Data JPA facilitates persistence with this model.

Implementing data access can be a hassle because we need to deal with connections, sessions, exception handling, and more, even for simple CRUD operations. That’s why the Spring Data JPA provides an additional level of functionality: creating repository implementations directly from interfaces and using conventions to generate queries from method names.
The following are some of the Spring Data JPA features.

    Support of the JPA specification with different providers, such as Hibernate, Eclipse Link, Open JPA, and so forth.

    Support for repositories (a concept from Domain-Driven Design).

    Auditing for domain class.

    Support for Quesydsl ( http://www.querydsl.com/ ) predicates and type-safe JPA queries.

    Pagination, sort, dynamic query execution support.

    Support for @Query annotations.

    Support for XML-based entity mapping.

    JavaConfig based repository configuration by using the @EnableJpaRepositories annotation.

Spring Data JPA with Spring Boot
One of the most important benefits from the Spring Data JPA is that we don’t need to worry about implementing basic CRUD functionalities, because that’s what it does. We only need to create an interface that extends from a Repository<T,ID>, CrudRepository<T,ID>, or JpaRepository<T,ID>. The JpaRepository interface offers not only what the CrudRepository does, but also extends from the PagingAndSortingRepository interface that provides extra functionality. If you review the CrudRepository<T,ID> interface (which you use in your ToDo app), you can see all the signature methods, as shown in Listing 5-3.
@NoRepositoryBean
public interface CrudRepository<T, ID> extends Repository<T, ID> {
      <S extends T> S save(S entity);
      <S extends T> Iterable<S> saveAll(Iterable<S> entities);
      Optional<T> findById(ID id);
      boolean existsById(ID id);
      Iterable<T> findAll();
      Iterable<T> findAllById(Iterable<ID> ids);
      long count();
      void deleteById(ID id);
      void delete(T entity);
      void deleteAll(Iterable<? extends T> entities);
      void deleteAll();
}
Listing 5-3

org.springframework.data.repository.CrudRepository.java

Listing 5-3 shows the CrudRepository<T,ID> interface, where the T means the entity (your domain model class) and the ID, the primary key that needs to implement Serializable.

In a simple Spring app, you are required to use the @EnableJpaRepositories annotation that triggers the extra configuration that is applied in the life cycle of the repositories defined within of your application. The good thing is that you don’t need this when using Spring Boot because Spring Boot takes care of it. Another feature from Spring Data JPA are the query methods, which are a very powerful way to create SQL statements with the fields of the domain entities.

So, to use Spring Data JPA with Spring Boot, you need spring-boot-starter-data-jpa and the SQL driver.

When Spring Boot executes its auto-configuration and finds out that you have the Spring Data JPA JAR, it configures the datasource by default (if there is none defined). It configures the JPA provider (by default it uses Hibernate). It enables the repositories (by using the @EnableJpaRepositories configuration). It checks if you have defined any query methods. And more.
ToDo App with Spring Data JPA

You can create your ToDo app from scratch, or take a look the classes you need, as well the necessary dependencies in your pom.xml or build.gradle files.
Starting from scratch, go to your browser and open Spring Initializr. Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-jpa

    Name: todo-jpa

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, JPA, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 5-4).

You can copy all the classes from the previous chapter, except for the ToDoRepository class, which is the only class that is new; you modify the others.
Repository: ToDoRepository
Create a ToDoRepository interface that extends from CrudRepository<T,ID>. The T is the ToDo class, and the ID is a String (see Listing 5-4).
package com.apress.todo.repository;
import com.apress.todo.domain.ToDo;
import org.springframework.data.repository.CrudRepository;
public interface ToDoRepository extends
                              CrudRepository<ToDo,String> {}
Listing 5-4

com.apress.todo.repository.ToDoRepository.java

Listing 5-4 shows the ToDoRepository interface that extends a CrudRepository. It is not necessary to create a concrete class or implement anything; the Spring Data JPA does the implementation for us. All the CRUD actions handle anything that we need to persist the data. That’s it—there’s nothing else that we need to do to use ToDoRepository where we need it.
Domain Model: ToDo
To use JPA and be compliant , it is necessary to declare the entity (@Entity) and the primary key (@Id) from the domain model. Let’s modify the ToDo class by adding the following annotations and methods (see Listing 5-5).
package com.apress.todo.domain;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Entity
@Data
@NoArgsConstructor
public class ToDo {
    @NotNull
    @Id
        @GeneratedValue(generator = "system-uuid")
        @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @NotNull
    @NotBlank
    private String description;
    @Column(insertable = true, updatable = false)
    private LocalDateTime created;
    private LocalDateTime modified;
    private boolean completed;
    public ToDo(String description){
        this.description = description;
    }
    @PrePersist
         void onCreate() {
        this.setCreated(LocalDateTime.now());
        this.setModified(LocalDateTime.now());
    }
    @PreUpdate
        void onUpdate() {
        this.setModified(LocalDateTime.now());
    }
}
Listing 5-5

com.apress.todo.domain.Todo.java
Listing 5-5 shows the modified version of the ToDo domain model . This class now has additional elements.

    @NoArgsConstructor. This annotation belongs to the Lombok library. It creates a class constructor with no arguments. It is required that JPA have a constructor with no arguments.

    @Entity. This annotation specifies that the class is an entity and is persisted into the database engine selected.

    @Id. This annotation specifies the primary key of an entity. The field annotated should be any Java primitive type and any primitive wrapper type.

    @GeneratedValue. This annotation provides the generation strategies for the values of primary keys (simple keys only). Normally, it is used with the @Id annotation. There are different strategies (IDENTIY, AUTO, SEQUENCE, and TABLE) and a key generator. In this case, the class defined the "system-uuid" (this generates a unique 36-character ID).

    @GenericGenerator. This is part of the Hibernate, which allows you to use the strategy to generate a unique ID from the previous annotation.

    @Column. This annotation specifies the mapped column for persistent properties; if there is no column annotation in a field, it is the default name of the column in the database. This class is marking the created field to be only for inserts but never for updates.

    @PrePersist. This annotation is a callback that is triggered before any persistent action is taken. It sets the new timestamp for the created and modified fields before the record is inserted into the database.

    @PreUpdate. This annotation is another callback that is triggered before any update action is taken. It sets the new timestamp for the modified field before it is updated into the database.

The last two annotations (@PrePersist and @PreUpdate) are a very nice way to deal with dates/timestamps , making it easier for the developer.

Before we continue, analyze the code to see what is different from previous versions of the ToDo domain model class.
Controller: ToDoController
Now, it’s time to modify the ToDoController class (see Listing 5-6).
package com.apress.todo.controller;
import com.apress.todo.domain.ToDo;
import com.apress.todo.domain.ToDoBuilder;
import com.apress.todo.repository.ToDoRepository;
import com.apress.todo.validation.ToDoValidationError;
import com.apress.todo.validation.ToDoValidationErrorBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder ;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;
@RestController
@RequestMapping("/api")
public class ToDoController {
    private ToDoRepository toDoRepository;
    @Autowired
    public ToDoController(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }
    @GetMapping("/todo")
    public ResponseEntity<Iterable<ToDo>> getToDos(){
        return ResponseEntity.ok(toDoRepository.findAll());
    }
    @GetMapping("/todo/{id}")
    public ResponseEntity<ToDo> getToDoById(@PathVariable String id){
        Optional<ToDo> toDo = toDoRepository.findById(id);
                 if(toDo.isPresent())
                     return ResponseEntity.ok(toDo.get());
                 return ResponseEntity.notFound().build();
    }
    @PatchMapping("/todo/{id}")
    public ResponseEntity<ToDo> setCompleted(@PathVariable String id){
        Optional<ToDo> toDo = toDoRepository.findById(id);
        if(!toDo.isPresent())
            return ResponseEntity.notFound().build();
        ToDo result = toDo.get();
        result.setCompleted(true);
        toDoRepository.save(result);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.ok().header("Location",location.toString()).build();
    }
    @RequestMapping(value="/todo", method = {RequestMethod.POST,RequestMethod.PUT})
    public ResponseEntity<?> createToDo(@Valid @RequestBody ToDo toDo, Errors errors){
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ToDoValidationErrorBuilder.fromBindingErrors(errors));
        }
        ToDo result = toDoRepository.save(toDo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }
    @DeleteMapping("/todo/{id}")
    public ResponseEntity<ToDo> deleteToDo(@PathVariable String id){
        toDoRepository.delete(ToDoBuilder.create().withId(id).build());
        return ResponseEntity.noContent().build();
    }
    @DeleteMapping("/todo")
    public ResponseEntity<ToDo> deleteToDo(@RequestBody ToDo toDo){
        toDoRepository.delete(toDo);
        return ResponseEntity.noContent().build();
    }
    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ToDoValidationError handleException(Exception exception) {
        return new ToDoValidationError(exception.getMessage());
    }
}
Listing 5-6

com.apress.todo.controller.ToDoController.java

Listing 5-6 shows the modified ToDoController class . It now directly uses the ToDoRepository interface, and some of the methods, like findById, return a Java 8 Optional type.

Before we continue, analyze the class to see what is different from previous versions. Most of the code remains the same.
Spring Boot JPA Properties

Spring Boot provides properties that allow you to override defaults when using the Spring Data JPA. One of them is the ability to create the DDL (data definition language), which is turned off by default, but you can enable it to do reverse engineering from your domain model. In other words, this property generates the tables and any other relationships from your domain model classes.

Also you can tell your JPA provider to create, drop, update, or validate your existing DDL/data, which is a useful migration mechanism. Also, you can set a property to show you the SQL statements that are being executed against the database engine.
Add the necessary properties to the application.properties file, as shown in Listing 5-7.
# JPA
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.show-sql=true
# H2
spring.h2.console.enabled=true
Listing 5-7

src/main/resources/application.properties
Listing 5-7 shows the application.properties and the JPA properties. It generates the table based on the domain model class and it creates the tables every time the application starts. The following are the possible values for the spring.jpa.hibernate.ddl-auto property.

    create (Creates the schema and destroys previous data).

    create-drop (Creates and then destroys the schema at the end of the session).

    update (Updates the schema, if necessary).

    validate (Validates the schema, makes no changes to the database).

    none (Disables DDL handling).

Running and Testing: ToDo App
Now it’s time to run and test the ToDo app. You can run it within your IDE. If you are using Maven, execute
./mvnw spring-boot:run
If you are using Gradle, execute
./gradlew bootRun

To test the ToDo app, you can run your ToDoClient app. It should work without any problems. Also you can send ToDo’s with the cURL command and see the H2 console (http://localhost:8080/h2-console).
Spring Data REST
The Spring Data REST project builds on top of the Spring Data repositories. It analyzes your domain model classes, and it exposes hypermedia-driven HTTP resources using HATEOAS (Hypermedia as the Engine of Application State, HAL +JSON). The following are some of the features.

    Exposes a discoverable RESTful API from your domain model classes using HAL as the media type.

    Supports pagination and exposes your domain class as collections.

    Exposes dedicated search resources for query methods defined in the repositories.

    Supports high customization for your own controllers if you want to extend the defaults.

    Allows hooking into the handling of REST requests by handling Spring ApplicationEvents.

    Brings a HAL browser to expose all the metadata; very useful for development purposes.

    Supports Spring Data JPA, Spring Data MongoDB, Spring Data Neo4j, Spring Data Solr, Spring Data Cassandra, and Spring Data Gemfire.

Spring Data REST with Spring Boot

If you want to use Spring Data REST in a regular Spring MVC app, you need to trigger its configuration by including the RepositoryRestMvcConfiguration class with the @Import annotation in your JavaConfig class (where you have your @Configuration annotation); but you don’t need to anything if you are using Spring Boot directly. Spring Boot takes care of this thanks to the @EnableAutoConfiguration annotation.

If you want to use the Spring Data REST in a Spring Boot application, you need to include the spring-boot-starter-data-rest and spring-boot-starter-data-* technology dependencies, and/or the SQL driver if you are going to use SQL database engines.
ToDo App with Spring Data JPA and Spring Data REST

You can create your ToDo app from scratch, or take a look which classes you need, as well the necessary dependencies, in your pom.xml or build.gradle files.
Starting from scratch, go to your browser and open Spring Initializr . Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-rest

    Name: todo-rest

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, JPA, REST Repositories, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 5-5).

You can copy only the domain model ToDo, ToDoRepository classes and the application.properties file; yes, only two classes and one properties file.
Running: ToDo App
Now it’s time to run and test the ToDo app. You can run it within your IDE. If you are using Maven, execute
./mvnw spring-boot:run
If you are using Gradle, execute
./gradlew bootRun
One of the important things to see is the output when running the ToDo app.
Mapped "{[/{repository}/search],methods=[HEAD],produces  ...
Mapped "{[/{repository}/search],methods=[GET],produces=  ...
Mapped "{[/{repository}/search],methods=[OPTIONS],produ  ...
Mapped "{[/{repository}/search/{search}],methods=[GET],  ...
Mapped "{[/{repository}/search/{search}],methods=[GET],  ...
Mapped "{[/{repository}/search/{search}],methods=[OPTIO  ...
Mapped "{[/{repository}/search/{search}],methods=[HEAD]  ...
Mapped "{[/{repository}/{id}/{property}],methods=[GET],  ...
Mapped "{[/{repository}/{id}/{property}/{propertyId}],m  ...
Mapped "{[/{repository}/{id}/{property}],methods=[DELET  ...
Mapped "{[/{repository}/{id}/{property}],methods=[GET],  ...
Mapped "{[/{repository}/{id}/{property}],methods=[PATCH  ...
Mapped "{[/{repository}/{id}/{property}/{propertyId}],m  ...
Mapped "{[/ || ],methods=[OPTIONS],produces=[applicatio  ...
Mapped "{[/ || ],methods=[HEAD],produces=[application/h  ...
Mapped "{[/ || ],methods=[GET],produces=[application/ha  ...
Mapped "{[/{repository}],methods=[OPTIONS],produces=[ap  ...
Mapped "{[/{repository}],methods=[HEAD],produces=[appli  ...
Mapped "{[/{repository}],methods=[GET],produces=[applic  ...
Mapped "{[/{repository}],methods=[GET],produces=[applic  ...
Mapped "{[/{repository}],methods=[POST],produces=[appli  ...
Mapped "{[/{repository}/{id}],methods=[OPTIONS],produce  ...
Mapped "{[/{repository}/{id}],methods=[HEAD],produces=[  ...
Mapped "{[/{repository}/{id}],methods=[GET],produces=[a  ...
Mapped "{[/{repository}/{id}],methods=[PUT],produces=[a  ...
Mapped "{[/{repository}/{id}],methods=[PATCH],produces=  ...
Mapped "{[/{repository}/{id}],methods=[DELETE],produces  ...
Mapped "{[/profile/{repository}],methods=[GET],produces  ...
Mapped "{[/profile/{repository}],methods=[OPTIONS],prod  ...
Mapped "{[/profile/{repository}],methods=[GET],produces  ...

It defines all the mapping endpoints of the repositories (only one in this app), and all the HTTP methods that you can use.
Testing: ToDo App

To test the ToDo app, we are going to use the cURL command and the browser. The ToDoClient app needs to be modified to accept the media type, HAL+JSON; so in this section, we won’t use it. First take a look at your browser. Go to the http://localhost:8080 you should see something similar to Figure 5-6.
First, take a look at your browser. Go to http://localhost:8080. You should see something similar to Figure 5-6.
../images/340891_2_En_5_Chapter/340891_2_En_5_Fig6_HTML.jpg
Figure 5-6

http://localhost:8080

If you see the same information but in RAW format, try to install the JSON Viewer plugin for your browser and reload the page. It exposes the http://localhost:8080/toDos URL as the endpoint, which means that you can access and execute all the HTTP methods (from the logs) to this URL.
Let’s add a few ToDo’s with a cURL command (is a single line).
curl -i -X POST -H "Content-Type: application/json" -d '{ "description":"Read the Pro Spring Boot 2nd Edition Book"}' http://localhost:8080/toDos
HTTP/1.1 201
Location: http://localhost:8080/toDos/8a8080876338ae4e016338b2e2ee0000
Content-Type: application/hal+json;charset=UTF-8
Transfer-Encoding: chunked
Date: Mon, 07 May 2018 03:43:57 GMT
{
  "description" : "Read the Pro Spring Boot 2nd Edition Book",
  "created" : "2018-05-06T21:43:57.676",
  "modified" : "2018-05-06T21:43:57.677",
  "completed" : false,
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/toDos/8a8080876338ae4e016338b2e2ee0000"
    },
    "toDo" : {
      "href" : "http://localhost:8080/toDos/8a8080876338ae4e016338b2e2ee0000"
    }
  }
}
You are getting a HAL+JSON result. After adding a couple more, you can go back to your browser and click the http://localhost:8080/toDos link. You see something like Figure 5-7.
../images/340891_2_En_5_Chapter/340891_2_En_5_Fig7_HTML.jpg
Figure 5-7

http://localhost:8080/toDos

Figure 5-7 shows the HAL+JSON response when accessing the /toDos endpoint.
Testing with HAL Browser: ToDo App

The Spring Data REST project has a tool—the HAL browser. It is a web app that helps developers visualize all the endpoints in an interactive way. So, if you don’t want to use the endpoints and/or cURL commands directly, you can use the HAL browser.
To use the HAL browser, add the following dependency. If you are using Maven, add the following to your pom.xml file.
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-rest-hal-browser</artifactId>
</dependency>

Maven pom.xml
If you are using Gradle, add the following to your build.gradle file.
compile 'org.springframework.data:spring-data-rest-hal-browser'

Gradle build.gradle
Now, you can restart your ToDo app and go directly to http://localhost:8080 in your browser. You the same as what’s shown in Figure 5-8.
../images/340891_2_En_5_Chapter/340891_2_En_5_Fig8_HTML.jpg
Figure 5-8

http://localhost:8080

You can click in the GET and NON-GET columns to interact with every single endpoint and HTTP method. This is a great alternative for a developer.

What I showed you is some of the many features that you can do with the Spring Data REST. You really don’t need any controller anymore. Also, you are able to expose any domain class, thanks to the easy override in Spring Boot and Spring Data REST.
No SQL Databases
NoSQL databases are another way to persist data, but in a different way than the tabular relationships of relational databases. There is already a classification system for these emergent NoSQL databases. You can find it based on its data model.

    Column (Cassandra, HBase, etc.)

    Document (CouchDB, MongoDB, etc.)

    Key-Value (Redis, Riak, etc.)

    Graph (Neo4J, Virtuoso, etc.)

    Multimodel (OrientDB, ArangoDB, etc.)

As you can see, you have many options. I think the most important kind of feature finds a database that is scalable and can easily handle millions of records.
Spring Data MongoDB

The Spring Data MongoDB project gives you the necessary interactions with the MongoDB document database. One of the important features is that you still work with domain model classes that use the @Document annotation and declare interfaces that use CrudRepository<T,ID>. This creates the necessary collection that the MongoDB uses for persistence.
The following are some of the features of this project.

    Spring Data MongoDB offer a MongoTemplate helper class (very similar to JdbcTemplate) that deals with all the boilerplate interacting with the MongoDB document database.

    Persistence and mapping lifecycle events.

    MongoTemplate helper class. It also provides low-level mapping using the MongoReader/MongoWriter abstractions.

    Java-based query, criteria, and update DSLs.

    Geospatial and MapReduce integrations and GridFS support.

    Cross-storage persistence support for JPA entities. This means that you can use your classes marked with @Entity and other annotations, and use them to persist/retrieve data using the MongoDB document database.

Spring Data MongoDB with Spring Boot

To use MongoDB with Spring Boot , you need to add the spring-boot-starter-data-mongodb dependency and have access to a MongoDB server instance.

Spring Boot uses the auto-configuration feature to set up everything to communicate with the MongoDB server instance. By default, Spring Boot tries to connect to the local host and use port 27017 (the MongoDB standard port). If you have a MongoDB remote server, you can connect to it by overriding the defaults. You need to use the spring.mongodb.* properties in the application.properties file (the easy way), or you can have a bean declaration using XML or in a JavaConfig class.

Spring Boot also auto-configures the MongoTemplate class (this class is very similar to JdbcTemplate), so it’s ready for any interaction with the MongoDB server. Another great feature is that you can work with repositories, meaning that you can reuse the same interface that you used for JPA.
MongoDB Installation

Before you start, you need to make sure that you have the MongoDB server installed on your computer.
If you are using Mac/Linux with the brew command ( http://brew.sh/ ), execute the following command.
brew install mongodb
You can run it with this command.
mongod

Or you can install MongoDB by downloading it from the website at https://www.mongodb.org/downloads#production and following the instructions.
MongoDB Embedded

There is another way to use MongoDB, at least as a development environment. You can use MongoDB Embedded. Normally, you use this in a test environment, but you can easily run it in development mode with a runtime scope.
To use MongoDB Embedded, you need to add the following dependency. If you are using Maven, you can add it into the pom.xml file.
<dependency>
      <groupId>de.flapdoodle.embed</groupId>
      <artifactId>de.flapdoodle.embed.mongo</artifactId>
      <scope>runtime</scope>
</dependency>

Maven pom.xml
If you are using Gradle:
runtime('de.flapdoodle.embed:de.flapdoodle.embed.mongo')

Gradle build.gradle
Next, you need to configure the Mongo client to use the MongoDB Embedded server (see Listing 5-8).
package com.apress.todo.config;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
@Configuration
public class ToDoConfig {
    private Environment environment;
    public ToDoConfig(Environment environment){
        this.environment = environment;
    }
    @Bean
         @DependsOn("embeddedMongoServer")
    public MongoClient mongoClient() {
        int port =
            this.environment.getProperty("local.mongo.port",
                                                Integer.class);
        return new MongoClient("localhost",port);
    }
}
Listing 5-8

com.apress.todo.config.ToDoConfig.java

Listing 5-8 shows the configuration of the MongoClient bean . MongoDB Embedded uses a random port when the application starts, that’s why it is necessary to also use the Environment bean.

If you take this approach to use a MongoDB server, you don’t need to set up any other properties.
ToDo App with Spring Data MongoDB

You can create your ToDo app from scratch, or take a look at which classes you need, as well the necessary dependencies, in your pom.xml or build.gradle files.
Starting from scratch, go to your browser and open Spring Initializr ( https://start.spring.io ). Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-mongo

    Name: todo-mongo

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, MongoDB, Embedded MongoDB

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 5-9).
../images/340891_2_En_5_Chapter/340891_2_En_5_Fig9_HTML.jpg
Figure 5-9

https://start.spring.io

You can copy all the classes from the todo-jpa project. In the next section, you see which classes need to be modified.
Domain Model: ToDo
Open the ToDo domain model class and modify it accordingly (see Listing 5-9).
package com.apress.todo.domain;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;
@Document
@Data
public class ToDo {
    @NotNull
    @Id
    private String id;
    @NotNull
    @NotBlank
    private String description;
    private LocalDateTime created;
    private LocalDateTime modified;
    private boolean completed;
    public ToDo(){
        LocalDateTime date = LocalDateTime.now() ;
        this.id = UUID.randomUUID().toString();
        this.created = date;
        this.modified = date;
    }
    public ToDo(String description){
        this();
        this.description = description;
    }
}
Listing 5-9

com.apress.todo.domain.ToDo.java

Listing 5-9 shows the modified ToDo class. The class is using the @Document annotation to be marked as persistent; it is also using @Id to declare a unique key.
If you have a remote MongoDB server, you can override the defaults that point at the local host. You can go to your application.properties file and add the following properties.
## MongoDB
spring.data.mongodb.host=my-remote-server
spring.data.mongodb.port=27017
spring.data.mongodb.username=myuser
spring.data.mongodb.password=secretpassword

Next, you can review your ToDoRepository and ToDoController classes, which should not change at all. That’s the beauty of using Spring Data : you can reuse your model for cross-store and all the previous classes, making development easier and faster.
Running and Testing: ToDo App
Now it’s time to run and test the ToDo app . You can run it within your IDE. Or if you are using Maven, execute
./mvnw spring-boot:run
If you are using Gradle, execute
./gradlew bootRun

To test the ToDo app, you can run your ToDoClient app—and that’s it. It is very easy to switch persistence engines without too much hassle. Maybe you are wondering what happen if you want to use map-reduce or lower-level operations. Well, you can by using the MongoTemplate class.
ToDo App with Spring Data MongoDB REST

How can you create a MongoDB REST app? You need to add the spring-boot-starter-data-rest dependency to your pom.xml or build.gradle files—and that’s it!! Of course, you need to remove the controller and validation packages and the ToDoBuilder class; you only need two classes.

Remember that Spring Data REST exposes the repository endpoints and uses HATEOAS as the media type (HAL+JSON).
Note

You can find the solution to this section in the book’s source code on the Apress website or on GitHub at https://github.com/Apress/pro-spring-boot-2 . The name of the project is todo-mongo-rest.
Spring Data Redis

Spring Data Redis provides an easy way to configure and access a Redis server. It offers low-level to high-level abstraction to interact with it, and follows the same Spring Data standard, providing a RedisTemplate class and repository-based persistence.
The following are some of the features of Spring Data Redis.

    RedisTemplate gives the high-level abstraction for all Redis operations.

    Messaging through Pub/Sub.

    Redis Sentinel and Redis Cluster support.

    Uses connection packages as a low level across multiple drivers, like Jedis and Lettuce.

    Repositories, sorting and paging by using @EnableRedisRepositories.

    Redis implements for Spring cache, so you can use Redis as your web cache mechanism.

Spring Data Redis with Spring Boot

If you want to use Spring Data Redis, you only need to add the spring-boot-starter-data-redis dependency to have access to a Redis server.

Spring Boot uses auto-configuration to set up defaults for using the Redis server. It automatically uses @EnableRedisRepositories (you don’t need to add it) if you are using the Repository feature.

By default, use the local host and port 6379. Of course, you can override the defaults by changing the spring.redis.* properties in the application.properties file.
ToDo App with Spring Data Redis

You can create your ToDo app from scratch, or take a look at which classes you need, as well the necessary dependencies, in your pom.xml or build.gradle files.
Starting from scratch, go to your browser and open Spring Initializr. Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-redis

    Name: todo-redis

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, Redis

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 5-10).
../images/340891_2_En_5_Chapter/340891_2_En_5_Fig10_HTML.jpg
Figure 5-10

https://start.spring.io

You can copy all the classes from the todo-mongo project. In the next section, we see which classes need to be modified.
Domain Model: ToDo
Open the ToDo domain model class and modified accordingly (see Listing 5-10).
package com.apress.todo.domain;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;
@Data
@RedisHash
public class ToDo {
    @NotNull
    @Id
    private String id;
    @NotNull
    @NotBlank
    private String description;
    private LocalDateTime created;
    private LocalDateTime modified ;
    private boolean completed;
    public ToDo(){
        LocalDateTime date = LocalDateTime.now();
        this.id = UUID.randomUUID().toString();
        this.created = date;
        this.modified = date;
    }
    public ToDo(String description){
        this();
        this.description = description;
    }
}
Listing 5-10

com.apress.todo.ToDo.java

Listing 5-10 shows the only class that you modify. The class is using the @RedisHash annotation that marks the class as persistent and also is using the @Id annotation as part of a composite key. When inserting a ToDo, there is a hash that contains a key with the format class:id. For this application, the composite key is something like "com.apress.todo.domain.ToDo: bbee6e32-37f3-4d5a-8f29-e2c79a28c301".
If you have a remote Redis server, you can override the defaults that point at the local host. You can go to your application.properties file and add the following properties.
## Redis - Remote
spring.redis.host=my-remote-server
spring.redis.port=6379
spring.redis.password=my-secret

You can review your ToDoRepository and ToDoController classes , which should not change at all; the same as before.
Running and Testing: ToDo App
Now it’s time to run and test this ToDo app . You can run it within your IDE. Or if you are using Maven, execute
./mvnw spring-boot:run
If you are using Gradle, execute
./gradlew bootRun

To test the ToDo app, you can run your ToDoClient app; and that’s it. If you want to use a different structure (like SET, LIST, STRING, ZSET) a low-level operations you can use the RedisTemplate class that is already setup and configured by Spring Boot.
Note

Remember that you can get the book’s source code from the Apress website or on GitHub at https://github.com/Apress/pro-spring-boot-2 .
More Data Features with Spring Boot

There are plenty more features and supported engines for manipulating data, from using a DSL with jOOQ (Java Object-Oriented Querying at www.jooq.org ), which generates Java code from your database and lets you build SQL queries through its own DSL in a type-safe way.

There are ways to do database migrations, using either Flyway ( https://flywaydb.org/ ) or Liquibase ( http://www.liquibase.org/ ) that you can run at startup.
Multiple Data Sources

One of the important features in Spring Boot (I believed it is a must-have feature) manipulates multiple DataSource instances, regardless of the persistent technology used.

As you already know, Spring Boot provides a default auto-configuration based on the app’s classpath, and you can override it without any problems. To use multiple DataSource instances, which may be pointing to different databases and/or different engines, you must override the defaults. If you remember from the Chapter 4, we created a complete but simple web app that required setup: DataSource, EntityManager, TransactionManager, JpaVendor, and so forth. Well, we need to add the same configuration if we want to use multiple datasources. In other words, we need to add multiple EntityManagers, TransactionManagers, and so forth.

How can we apply this to the ToDo app? Review what you did in Chapter 4. Take a close look at each configuration, and you can imagine what you need to do.

You can get the solution in the book’s source code. The name of the project is todo-rest-2ds. This project contains all the User and the ToDo domain classes that persist data into their own databases.
Summary

In this chapter, you learned different data technologies and how they work with Spring Boot. You also learned that Spring Boot uses its auto-configuration feature to apply defaults based on the classpath.

You learned that if you have two or more SQL drivers and one of them is H2, HSQL or Derby, Spring Boot configures the embedded database engine if you haven’t defined a DataSource instance yet. You saw how to configure and override some of the data defaults. You learned that Spring Data implements a Template pattern to hide all the complex tasks that normally do without the Spring Framework.

In the next chapter, we take the web and data up one level with reactive programming and explore the WebFlux and reactive data.