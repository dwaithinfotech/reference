What Is Messaging?

Messaging is a way of communicating among one or more entities, and it is everywhere.

Computer messaging, in one form or another, has been around since the invention of the computer. It is defined as a method of communication between hardware and/or software components or applications. There is always a sender and one or more receivers. Messaging can be synchronous and asynchronous, pub/sub and peer-to-peer, RPC, enterprise-based, a message broker, ESB (enterprise service bus), MOM (message-oriented middleware), and so forth.

Messaging enables distributed communication that must be loosely coupled, meaning that it doesn’t matter how or what message the sender is publishing, the receiver consumes the message without telling the sender.

Of course, there is a lot we could say about messaging—from the old techniques and technologies to new protocols and messaging patterns, but the intention of this chapter is to work with examples that illustrate how Spring Boot does messaging.

With this in mind, let’s start creating examples using some of the technologies and message brokers out there.
JMS with Spring Boot

Let’s start by using JMS. This is an old technology that is still being used by companies with legacy applications. JMS was created by Sun Microsystems to enable a way to send messages synchronously and asynchronously; it defines interfaces that need to be implemented by message brokers, such as WebLogic, IBM MQ, ActiveMQ, HornetQ, and so forth.

JMS is a Java-only technology, and so there have been attempts to create message bridges to combine JMS with other programming languages; still, it’s difficult or very expensive to mix different technologies. I know that you are thinking that this is not true, because you can use Spring integration, Google Protobuffers, Apache Thrift, and other technologies to integrate JMS, but it’s still a lot of work because you need to know and maintain code from all of these technologies.
ToDo App with JMS

Let’s start by creating the ToDo App using JMS with Spring Boot. The idea is to send ToDo’s to a JMS broker and receive and save them.

The Spring Boot team has several JMS starter poms available; in this case, you use ActiveMQ, which is an open source asynchronous messaging broker from the Apache Foundation ( http://activemq.apache.org ). One of the main advantages is that you can use either the in-memory broker or a remote broker. (You can download it and install it if you prefer; the code in this section uses the in-memory broker, but I tell you how to configure a remote broker).
You can open your favorite browser and point to the known Spring Initializr ( https://start.spring.io ); add the following values to the following fields.

    Group: com.apress.todo

    Artifact: todo-jms

    Name: todo-jms

    Package Name: com.apress.todo

    Dependencies: JMS (ActiveMQ), Web, Lombok, JPA, REST Repositories, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button to download a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 9-1).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig1_HTML.jpg
Figure 9-1

Spring Initializr

As you can see from the dependencies, you reuse the JPA and REST Repositories code from previous chapters. Instead of using Text message (a common approach for testing messaging) you use a ToDo instance, and it is converted as JSON format. to do this you required to add manually the next dependency to your pom.xml or build.gradle.
If you are using Maven, add the following dependency to your pom.xml file .
<dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
</dependency>
If you are using Gradle, add the following dependency to your build.gradle file .
compile("com.fasterxml.jackson.core:jackson-databind")

This dependency provides all the Jackson jars needed to use JSON to serialize the ToDo entity.

In the following sections I show you the important files, and how JMS is used in the ToDo app. The example uses the simple Point-to-Point pattern, where there is a Producer, a Queue and a Consumer. I’ll show later on how to configure it for using a Publisher-Subscriber pattern with a Producer, a Topic and multiple Consumers later on.
ToDo Producer

Let’s start by introducing the Producer that sends a ToDo to the ActiveMQ broker. This producer can be on its own project; it can be separated from the app; but for demonstration purposes you have the producer in the same code base, in the ToDo app.
Create the ToDoProducer class . This class sends a ToDo into a JMS Queue (see Listing 9-1).
package com.apress.todo.jms;
import com.apress.todo.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
@Component
public class ToDoProducer {
    private static final Logger log = LoggerFactory.getLogger(ToDoProducer.class);
    private JmsTemplate jmsTemplate;
    public ToDoProducer(JmsTemplate jmsTemplate){
        this.jmsTemplate = jmsTemplate;
    }
    public void sendTo(String destination, ToDo toDo) {
        this.jmsTemplate.convertAndSend(destination, toDo);
        log.info("Producer> Message Sent");
    }
}
Listing 9-1

com.apress.todo.jms.ToDoProducer.java

Listing 9-1 shows the producer class. This class is marked using the @Component, so it is registered as a Spring bean in the Spring application context. The JmsTemplate class is used, and it is very similar to other *Template classes that wrap all the boilerplate of the technology in use. The JmsTemplate instance is injected through the class constructor, and it is used to send a message using the convertAndSend method. You are sending a ToDo object (JSON String). This template has the mechanism to serialize it and send it to the ActiveMQ queue.
ToDo Consumer
Next, let’s create the consumer class, which is listening for any incoming message from the ActiveMQ queue (see Listing 9-2).
package com.apress.todo.jms;
import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.ToDoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import javax.validation.Valid;
@Component
public class ToDoConsumer {
    private Logger log = LoggerFactory.getLogger(ToDoConsumer.class);
    private ToDoRepository repository;
    public ToDoConsumer(ToDoRepository repository){
        this.repository = repository;
    }
    @JmsListener(destination = "${todo.jms.destination}",containerFactory = "jmsFactory")
    public void processToDo(@Valid ToDo todo){
        log.info("Consumer> " + todo);
        log.info("ToDo created> " + this.repository.save(todo));
    }
}
Listing 9-2

com.apress.todo.jms.ToDoConsumer.java

Listing 9-2 shows the consumer. In this class, you are using ToDoRepository, where it is listening for any message from the ActiveMQ queue. Make sure that you are using the @JmsListener annotation that makes the method process any incoming message from the queue; in this case, a valid ToDo (the @Valid annotation can be used to validate any field of the domain model). The @JmsListener annotation has two attributes. The destination attribute emphasizes the name of the queue/topic to connect to (the destination attribute evaluate the todo.jms.destination property, which you create/use in the next section). The containerFactory attribute is created as part of the configuration.
Configuring the ToDo App

Now, it is time to configure the ToDo App to send and receive ToDo’s. Listing 9-1 and Listing 9-2 show the Producer and Consumer classes, respectively. In both classes you are using a ToDo instance, meaning that it is necessary to do serialization. Most of the Java frameworks that work with serialization require that your classes implement from java.io.Serializable. It is an easy way to convert those classes into bytes, but this approach has been debated for years because implementing Serializable decreases the flexibility to modify a class’s implementation once it’s released for usage.

The Spring Framework offers an alternative way to do serialization without implementing from Serializable—through a MessageConverter interface. This interface offers the toMessage and fromMessage methods, in which you can plug in whatever technology fits for object conversion.
Let’s create a configuration that uses a ToDo instance for the producer and consumer (see Listing 9-3).
package com.apress.todo.config;
import com.apress.todo.error.ToDoErrorHandler;
import com.apress.todo.validator.ToDoValidator;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import javax.jms.ConnectionFactory;
@Configuration
public class ToDoConfig {
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_class_");
        return converter;
    }
    @Bean
    public JmsListenerContainerFactory<?> jmsFactory(ConnectionFactory connectionFactory,
                                                     DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setErrorHandler(new ToDoErrorHandler());
        configurer.configure(factory, connectionFactory);
        return factory;
    }
    @Configuration
    static class MethodListenerConfig implements JmsListenerConfigurer{
        @Override
        public void configureJmsListeners (JmsListenerEndpointRegistrar jmsListenerEndpointRegistrar){
            jmsListenerEndpointRegistrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
        }
        @Bean
        public DefaultMessageHandlerMethodFactory myHandlerMethodFactory () {
            DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
            factory.setValidator(new ToDoValidator());
            return factory;
        }
    }
}
Listing 9-3

com.apress.todo.config.ToDoConfig.java
Listing 9-3 shows the ToDoConfig class that is use for the app. Let’s analyze it.

    @Configuration. This is a known annotation that marks the class for configuring the SpringApplication context.

    MessageConverter. The method jacksonJmsMessageConverter returns the MessageConverter interface. This interface promotes the implementation of toMessage and fromMessage method that helps plug in any serialization/conversion that you want to use. In this case, you are using a JSON converter by using the MappingJackson2MessageConverter class implementation. This class is one of the default implementations that you can find in the Spring Framework. It uses the Jackson libraries, which use mappers to convert to/from JSON to/from an object. Because you are working with ToDo instances, it is necessary to specify a target type (setTargetType), this means that the JSON object is taken as text and a type-id property name (setTypeIdPropertyName) that identifies a property found from/to the producer and consumer. The type-id property name must always match both the producer and consumer. It can be any value you need (preferably something that you recognize because it is used to set the name (including the package) of the class to be converted to/from JSON); in other words, the com.apress.todo.domain.Todo class must be shared between the producer and consumer so that the mapper knows where to get the class to/from.

    JmsListenerContainerFactory. The jmsFactory method returns JmsListenerContainerFactory. This bean requires ConnectionFactory and DefaultJmsListenerContainerFactoryConfigurer (both is injected by the Spring), and it creates DefaultJmsListenerContainerFactory, which sets up an error handler. This bean is used in the @JmsListener annotation by setting the containerFactory attribute.

    JmsListenerConfigurer. In this class, you are creating a static configuration. The MethodListenerConfig class implements the JmsListenerConfigurer interface. This interface requires you to register a bean that has the configuration of the validator (ToDoValidator class); in this case, the DefaultMessageHandlerMethodFactory bean.

If you don’t want to validate yet, you can remove the MethodListenerConfig class and the setErrorHandler call from the jmsFactory bean declaration; but if you want to experiment with validation, then you need to create the ToDoValidator class (see Listing 9-4).
package com.apress.todo.validator;
import com.apress.todo.domain.ToDo;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
public class ToDoValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(ToDo.class);
    }
    @Override
    public void validate(Object target, Errors errors) {
        ToDo toDo = (ToDo)target;
        if (toDo == null) {
            errors.reject(null, "ToDo cannot be null");
        }else {
            if (toDo.getDescription() == null || toDo.getDescription().isEmpty())
                errors.rejectValue("description",null,"description cannot be null or empty");
        }
    }
}
Listing 9-4

com.apress.todo.validator.ToDoValidator.java

Listing 9-4 shows the validator class that is called for every message, and validates that the description field is not empty or null. This class is implementing the Validator interface and implements the supports and validate methods.
This is the ToDoErrorHandler code .
package com.apress.todo.error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;
public class ToDoErrorHandler implements ErrorHandler {
    private static Logger log = LoggerFactory.getLogger(ToDoErrorHandler.class);
    @Override
    public void handleError(Throwable t) {
        log.warn("ToDo error...");
        log.error(t.getCause().getMessage());
    }
}

As you can see, this class is implementing the ErrorHandler interface.
Now, let’s create the ToDoProperties class that holds the todo.jms.destination property, which indicates which queue/topic to connect to (see Listing 9-5).
package com.apress.todo.config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "todo.jms")
public class ToDoProperties {
    private String destination;
}
Listing 9-5

com.apress.todo.config.ToDoProperties.java

Listing 9-5 shows the ToDoProperties class. Remember that in Listing 9-2 (the ToDoConsumer class), the processToDo method was marked with the @JmsListener annotation, which exposed the destination attribute. This attribute gets its value from evaluating the SpEL (Spring Expression Language) ${todo.jms.destination} expression that you are defining within this class.
You can set this property in the application.properties file .
# JPA
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=create-drop
# ToDo JMS
todo.jms.destination=toDoDestination

src/main/resources/application.properties
Running the ToDo App
Next, let’s create a config class that sends a message to the Queue using the producer (see Listing 9-6).
package com.apress.todo.config;
import com.apress.todo.domain.ToDo;
import com.apress.todo.jms.ToDoProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class ToDoSender {
    @Bean
    public CommandLineRunner sendToDos(@Value("${todo.jms.destination}") String destination, ToDoProducer producer){
        return args -> {
            producer.sendTo(destination,new ToDo("workout tomorrow morning!"));
        };
    }
}
Listing 9-6

com.apress.todo.config.ToDoSender.java

Listing 9-6 shows the config class that sends the message using a ToDoProducer instance and the destination (from the todo.jms.destination property).
To run the app, you can either use your IDE (if you imported it) or you can use the Maven wrapper.
./mvnw spring-boot:run
Or the Gradle wrapper.
./gradlew bootRun
You should get the following text from the logs.
Producer> Message Sent
Consumer> ToDo(id=null, description=workout tomorrow morning!, created=null, modified=null, completed=false)
ToDo created> ToDo(id=8a808087645bd67001645bd6785b0000, description=workout tomorrow morning!, created=2018-07-02T10:32:19.546, modified=2018-07-02T10:32:19.547, completed=false)

You can take a look at http://localhost:8080/toDos and see the ToDo created.
Using JMS Pub/Sub

If you want to use the Pub/Sub pattern, where you want to have multiple consumers receiving a message (by using a topic for subscription), I’ll explain what you need to do in your app.

Because we are using Spring Boot, this makes it easier to configure a Pub/Sub pattern. If you are using default listener (a @JmsListener(destination) default listener container), then you can use the spring.jms.pub-sub-domain=true property in the application.properties file.
But if you are using a custom listener container, then you can set it programmatically.
@Bean
public DefaultMessageListenerContainer jmsListenerContainerFactory() {
    DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
    dmlc.setPubSubDomain(true);
    // Other configuration here ...
    return dmlc;
}
Remote ActiveMQ
The ToDo App is using the in-memory broker (spring.activemq.in-memory=true). This is probably good for demos or testing, but in reality, you use a remote ActiveMQ server. If you required a remote server, add the following keys to your application.properties file (modify it accordingly).
spring.activemq.broker-url=tcp://my-awesome-server.com:61616
spring.activemq.user=admin
spring.activemq.password=admin

src/main/resources/application.properties

There are many more properties that you can use for the ActiveMQ broker. Go to https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html and look for the spring.activemq.* keys.
RabbitMQ with Spring Boot

Since the first attempts with JMS by companies like Sun, Oracle, and IBM, and Microsoft with MSMQ, the protocols used were proprietary. JMS defines an Interface API, but trying to mix technologies or programming languages is a hassle. Thanks to a JPMorgan team, the AMQP (Advance Message Queuing Protocol) was created. It’s an open standard application layer for MOM. In other words, AMQP is a wire-level protocol, meaning that you can use any technology or programming language with this protocol.

Messaging brokers compete with each other to prove that they are robust, reliable, and scalable, but the most important issue is how fast they are. I’ve been working with a lot of brokers, and so far one of the easiest to use and to scale, and the fastest, is RabbitMQ, which implements the AMQP protocol.

It would take an entire book to describe each part of RabbitMQ and all the concepts around it, but I’ll try to explain some of them based on this section’s example.
Installing RabbitMQ
Before I talk about RabbitMQ, let’s install it. If you are using Mac OS X/Linux, you can use the brew command.
$ brew upgrade
$ brew install rabbitmq

If you are using a UNIX or a Windows system, you can go to the RabbitMQ web site and use the installers ( www.rabbitmq.com/download.html ). RabbitMQ is written in Erlang, so its major dependency is to install the Erlang runtime in your system. Nowadays, all the RabbitMQ installers come with all the Erlang dependencies. Make sure the executables are in your PATH variable (for Windows and Linux, depending of what OS you are using). If you are using brew, you don’t need to worry about setting the PATH variable.
RabbitMQ/AMQP: Exchanges, Bindings, and Queues

The AMQP defines three concepts that are a little different from the JMS world, but very easy to understand. AMQP defines exchanges, which are entities where the messages are sent. Every exchange takes a message and routes it to zero or more queues. This routing involves an algorithm that is based on the exchange type and rules, called bindings.
The AMPQ protocol defines five exchange types: Direct, Fanout, Topic, and Headers. Figure 9-2 shows these different exchange types.
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig2_HTML.jpg
Figure 9-2

AMQP exchanges/bindings/ queues

Figure 9-2 shows the possible exchange types. So, the main idea is to send a message to an exchange, including a routing key, then the exchange based on its type deliver the message to the queue (or it won’t if the routing key doesn’t match).

The default exchange is bound automatically to every queue created. The direct exchange is bound to a queue by a routing key; you can see this exchange type as one-to-one binding. The topic exchange is similar to the direct exchange; the only difference is that in its binding, you can add a wildcard into its routing key. The headers exchange is similar to the topic exchange; the only difference is that the binding is based on the message headers (this is a very powerful exchange, and you can do all and any expressions for its headers). The fanout exchange copy the message to all the bound queues; you can see this exchange as a message broadcast.

You can get more information about these topics at www.rabbitmq.com/tutorials/amqp-concepts.html .

The example in this section uses the default exchange type, which means that the routing key is equal to the name of the queue. Every time you create a queue, RabbitMQ creates a binding from the default exchange (the actual name is an empty string) to the queue using the queue’s name.
ToDo App with RabbitMQ

Let’s retake the ToDo app and add an AMQP messaging. The same as with the previous app, you work with ToDo instances. You send and receive a JSON message and convert it to object.
Let’s start by opening your favorite browser and point to the known Spring Initializr. Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-rabbitmq

    Name: todo-rabbitmq

    Package Name: com.apress.todo

    Dependencies: RabbitMQ, Web, Lombok, JPA, REST Repositories, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 9-3).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig3_HTML.jpg
Figure 9-3

Spring Initializr https://start.spring.io

You can copy/paste the code of the JPA/REST project from previous chapters.
ToDo Producer
Lets’ start by creating a Producer class that sends messages to Exchange (Default Exchange—direct) (see Listing 9-7).
package com.apress.todo.rmq;
import com.apress.todo.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
@Component
public class ToDoProducer {
    private static final Logger log = LoggerFactory.getLogger(ToDoProducer.class);
    private RabbitTemplate template;
    public ToDoProducer(RabbitTemplate template){
        this.template = template;
    }
    public void sendTo(String queue, ToDo toDo){
        this.template.convertAndSend(queue,toDo);
        log.info("Producer> Message Sent");
    }
}
Listing 9-7

com.apress.todo.rmq.ToDoProducer.java
Listing 9-7 shows the ToDoProducer.java class. Let’s examine it.

    @Component. This annotation marks the class to be picked up by the Spring container.

    RabbitTemplate. The RabbitTemplate is a helper class that simplifies synchronous/asynchronous access to RabbitMQ for sending and/or receiving messages. This is very similar to the JmsTemplate you saw earlier.

    sendTo(routingKey,message). This method has the routing key and the message as parameters. In this case, the routing key is the name of the queue. This method uses the rabbitTemplate instance to call the convertAndSend method that accepts the routing key and the message. Remember that the message is sent to the exchange (the default exchange) and the exchange routes the message to the right queue. This routing key happens to be the name of the queue. Also remember that by default RabbitMQ always binds the default exchange (Direct Exchange) to a queue and the routing key is the queue’s name.

ToDo Consumer
Next, it’s time to create the Consumer class that is listening to the specified queue (see Listing 9-8).
package com.apress.todo.rmq;
import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.ToDoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
@Component
public class ToDoConsumer {
    private Logger log = LoggerFactory.getLogger(ToDoConsumer.class);
    private ToDoRepository repository;
    public ToDoConsumer(ToDoRepository repository){
        this.repository = repository;
    }
    @RabbitListener(queues = "${todo.amqp.queue}")
    public void processToDo(ToDo todo){
        log.info("Consumer> " + todo);
        log.info("ToDo created> " + this.repository.save(todo));
    }
}
Listing 9-8

com.apress.todo.rmq.ToDoConsumer.java
Listing 9-8 shows the ToDoConsumer.java class. Let’s examine it.

    @Component. You already know this annotation. It marks the class to be picked up by the Spring container.

    @RabbitListener. This annotation marks the method (because you can use this annotation in a class as well) for creating a handler for any incoming messages, meaning that it creates a listener that is connected to the RabbitMQ’s queue and passes that message to the method. Behind the scenes, the listener does its best to convert the message to the appropriate type by using the right message converter (an implementation of the org.springframework.amqp.support.converter.MessageConverter interface. This interface belongs to the spring-amqp project); in this case, it converts from JSON to a ToDo instance.

As you can see from the ToDoProducer and ToDoConsumer, the code is very simple. If you created this by only using the RabbitMQ Java client ( www.rabbitmq.com/java-client.html ), at least you need more lines of code to create a connection, a channel, and a message and send the message, or if you are writing a consumer, then you need to open a connection, create a channel, create a basic consumer, and get into a loop for processing every incoming message. This is a lot for simple producers or consumers. That’s why the Spring AMQP team created this simple way to do a heavy task in a few lines of code.
Configuring the ToDo App
Next let’s configure the app. Remember that you are sending ToDo instances, so practically, it is kind of the same configuration that we did with JMS. We need to set our converter and the listener container (see Listing 9-9).
package com.apress.todo.config;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class ToDoConfig {
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(new Jackson2JsonMessageConverter());
        return template;
    }
    @Bean
    public Queue queueCreation(@Value("${todo.amqp.queue}") String queue){
        return new Queue(queue,true,false,false);
    }
}
Listing 9-9

com.apress.todo.config.ToDoConfig.java
Listing 9-9 shows you the configuration. It has several bean definitions; let’s examine them.

    SimpleRabbitListenerContainerFactory. This factory is required when using the @RabbitListener annotation for custom setup because you are working with ToDo instances; it is necessary set the message converter.

    Jackson2JsonMessageConverter. This converter is used for producing (with the RabbitTemplate) and for consuming (@RabbitListener); it uses the Jackson libraries for doing its mapping and conversion.

    RabbitTemplate. This a helper class that can send and receive messages. In this case, it is necessary to customize it to produce JSON objects using the Jackson converter.

    Queue. You can manually create a queue, but in this case, you are creating it programmatically. You pass the name of the queue, if is going to be durable or exclusive, and auto-delete.

Remember that in the AMQP protocol, you need an exchange that is bound to a queue, so this particular example creates at runtime a queue named spring-boot, and by default, all the queues are bound to a default exchange. That’s why you didn’t provide any information about an exchange. So, when the producer sends the message, it is sent first to the default exchange and then routed to the queue (spring-boot).
Running the ToDo App
Let’s create the sender class that sends the ToDo message (see Listing 9-10).
package com.apress.todo.config;
import com.apress.todo.domain.ToDo;
import com.apress.todo.rmq.ToDoProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class ToDoSender {
    @Bean
    public CommandLineRunner sendToDos(@Value("${todo.amqp.queue}") String destination, ToDoProducer producer){
        return args -> {
            producer.sendTo(destination,new ToDo("workout tomorrow morning!"));
        };
    }
}
Listing 9-10

com.apress.todo.config.ToDoSender.java
Add the following keys (that declare the queue to send-to/consume-from) to your application.properties file .
todo.amqp.queue=spring-boot
Before you run your example, make sure your RabbitMQ server is up and running. You can start it by opening a terminal and executing the following command.
$ rabbitmq-server
Make sure that you have access to the RabbitMQ web console by going to http://localhost:15672/ with guest/guest credentials. If you have problems accessing the web console, make sure that you have the Management plugin enabled by running the following command.
$ rabbitmq-plugins list
If the entire list has the boxes unchecked, then the Management plugin is not enabled yet (normally happens with fresh installations). To enable this plugin, you can execute the following command.
$ rabbitmq-plugins enable rabbitmq_management --online
Now, you can try again. You should then see a web console similar to Figure 9-4.
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig4_HTML.jpg
Figure 9-4

RabbitMQ web console management
Figure 9-4 shows the RabbitMQ web console. Now you can run the project as usual, using your IDE. If you are using Maven, execute
$ ./mvnw spring-boot:run
If you are using Gradle, execute
$./gradlew bootRun
After you execute this command, you should have something similar to the following output.
Producer> Message Sent
Consumer> ToDo(id=null, description=workout tomorrow morning!, created=null, modified=null, completed=false)
ToDo created> ToDo(id=8a808087645bd67001645bd6785b0000, description=workout tomorrow morning!, created=2018-07-02T10:32:19.546, modified=2018-07-02T10:32:19.547, completed=false)
If you take a look at the RabbitMQ web console in the Queues tab, you should have defined the spring-boot queue (see Figure 9-5).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig5_HTML.jpg
Figure 9-5

RabbitMQ web console Queues tab
Figure 9-5 shows the Queues tab from the RabbitMQ web console. The message you sent was delivered right away. If you want to play a little more and see a part of the throughput, you can modify the ToDoSender class as shown in Listing 9-11, but don’t forget to stop your app.
package com.apress.todo.config;
import com.apress.todo.domain.ToDo;
import com.apress.todo.rmq.ToDoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import java.text.SimpleDateFormat;
import java.util.Date;
@EnableScheduling
@Configuration
public class ToDoSender {
    @Autowired
    private ToDoProducer producer;
    @Value("${todo.amqp.queue}")
    private String destination;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    @Scheduled(fixedRate = 500L)
    private void sendToDos(){
        producer.sendTo(destination,new ToDo("Thinking on Spring Boot at " + dateFormat.format(new Date())));
    }
}
Listing 9-11

Version 2 of com.apress.todo.config.ToDoSender.java
Listing 9-11 shows a modified version of the ToDoSender class . Let’s examine this new version.

    @EnableScheduling. This annotation tells (via auto-configuration) the Spring container that the org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor class needs to be created. It registers all the methods annotated with @Scheduled to be invoked by an org.springframework.scheduling.TaskScheduler interface implementation according to the fixedRate, fixedDelay, or cron expression in the @Scheduled annotation.

    @Scheduled(fixedDelay = 500L). This annotation tells the TaskScheduler interface implementation to execute the sendToDos method with a fixed delay of 500 milliseconds. This means that every half second you send a message to the queue.

The other part of the app you already know. So if you execute the project again, you should see endless messaging. While this is running, take a look at the RabbitMQ console and see the output. You can put a for loop to send more messages in a half second.
Remote RabbitMQ
If you want to access a remote RabbitMQ, you add the following properties to the application.properties file.
spring.rabbitmq.host=mydomain.com
spring.rabbitmq.username=rabbituser
spring.rabbitmq.password=thisissecured
spring.rabbitmq.port=5672
spring.rabbitmq.virtual-host=/production

You can always read about all the properties for RabbitMQ in the Spring Boot reference at https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html .

Now you know how easy it is to use RabbitMQ with Spring Boot. If you want to learn more about RabbitMQ and the Spring AMQP technology, you can get more information at the main projects web site at http://projects.spring.io/spring-amqp/ .

You can stop RabbitMQ by pressing Ctrl+C, where you start the broker. There are more options on how to use RabbitMQ, like creating a cluster or having high availability. You can learn more information about it at www.rabbitmq.com .
Redis Messaging with Spring Boot

Now it’s Redis’ turn. Redis (REmote DIctionary Server) is a NoSQL key-value store database. It’s written in C, and even though it has a small footprint in its core, it’s very reliable, scalable, powerful, and super fast. Its primary function is to store data structures, such as lists, hashes, strings, sets, and sorted sets. A main feature provides a publish/subscribe messaging system, which is why you are going to use Redis as the message broker.
Installing Redis
Installing Redis is very simple. If you are using Mac OS X/Linux, you can use brew and execute the following.
$ brew update && brew install redis

If you are using a different flavor of UNIX or Windows, you can go to the Redis web site and download the Redis installers at http://redis.io/download . Or if you want to compile it according to your system, you can do that as well by downloading the source code.
ToDo App with Redis

Using Redis for Pub/Sub messaging is very simple and very similar to other technologies. You send and receive ToDo’s using the Pub/Sub messaging pattern with Redis.
Let’s start by opening your favorite browser and point to Spring Initializr . Add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-redis

    Name: todo-redis

    Package Name: com.apress.todo

    Dependencies: Redis, Web, Lombok, JPA, REST Repositories, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 9-6).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig6_HTML.jpg
Figure 9-6

Spring Initializr

You use the ToDo domain and repo from previous chapters.
ToDo Producer
Let’s create the Producer class that sends a Todo instance to a specific topic (see Listing 9-12).
package com.apress.todo.redis;
import com.apress.todo.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
@Component
public class ToDoProducer {
    private static final Logger log = LoggerFactory.getLogger(ToDoProducer.class);
    private RedisTemplate redisTemplate;
    public ToDoProducer(RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }
    public void sendTo(String topic, ToDo toDo){
        log.info("Producer> ToDo sent");
        this.redisTemplate.convertAndSend(topic, toDo);
    }
}
Listing 9-12

com.apress.todo.redis.ToDoProducer.java

Listing 9-12 shows the Producer class. It is very similar to previous technologies. It uses a *Template pattern class; in this case, the RedisTemplate that sends ToDo instances to a specific topic.
ToDo Consumer
Next, create the consumer that subscribes to the topic (see Listing 9-13).
package com.apress.todo.redis;
import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.ToDoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
@Component
public class ToDoConsumer {
    private static final Logger log = LoggerFactory.getLogger(ToDoConsumer.class);
    private ToDoRepository repository;
    public ToDoConsumer(ToDoRepository repository){
        this.repository = repository;
    }
    public void handleMessage(ToDo toDo) {
        log.info("Consumer> " + toDo);
        log.info("ToDo created> " + this.repository.save(toDo));
    }
}
Listing 9-13

com.apress.todo.redis.ToDoConsumer.java

Listing 9-13 shows the consumer that is subscribed to the topic for any incoming ToDo messages. It is important to know that it is mandatory to have a handleMessage method name to use the listener (this is a constraint when creating a MessageListenerAdapter).
Configuring the ToDo App
Next, let’s create the configuration for the ToDo app (see Listing 9-14).
package com.apress.todo.config;
import com.apress.todo.domain.ToDo;
import com.apress.todo.redis.ToDoConsumer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
@Configuration
public class ToDoConfig {
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                                   MessageListenerAdapter toDoListenerAdapter, @Value("${todo.redis.topic}") String topic) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(toDoListenerAdapter, new PatternTopic(topic));
        return container;
    }
    @Bean
    MessageListenerAdapter toDoListenerAdapter(ToDoConsumer consumer) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(consumer);
        messageListenerAdapter.setSerializer(new Jackson2JsonRedisSerializer<>(ToDo.class));
        return messageListenerAdapter;
    }
    @Bean
    RedisTemplate<String, ToDo> redisTemplate(RedisConnectionFactory connectionFactory){
        RedisTemplate<String,ToDo> redisTemplate = new RedisTemplate<String,ToDo>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setDefaultSerializer(new Jackson2JsonRedisSerializer<>(ToDo.class));
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
}
Listing 9-14

com.apress.todo.config.ToDoConfig.java
Listing 9-14 shows the configuration needed for the ToDo app. This class declares the following Spring beans.

    RedisMessageListenerContainer. This class is in charge of connecting to the Redis topic.

    MessageListenerAdapter. This adapter takes a POJO (Plain Old Java Object) class to process the message. As a requirement, the method must be named handleMessage; this method receives the message from the topic as a ToDo instance, which is why it also requires a serializer.

    Jackson2JsonRedisSerializer. This serializer converts from/to the ToDo instance.

    RedisTemplate. This class implements the Template pattern and is very similar to the other messaging technologies. This class requires a serializer to work with JSON and to/from ToDo instances.

This customization is needed to work with the JSON format and do the right conversion to/from ToDo instances; but you can avoid everything and use the default configuration that requires a serializable object (like a String) to send and use the StringRedisTemplate instead.
In the application.properties file, add the following content.
# JPA
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=create-drop
# ToDo Redis
todo.redis.topic=todos
Running the ToDo App
Before you run the ToDo app, make sure that you have the Redis server up and running. To start it, execute the following command in a terminal.
$ redis-server
89887:C 11 Feb 20:17:55.320 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
89887:M 11 Feb 20:17:55.321 * Increased maximum number of open files to 10032 (it was originally set to 256).
                _._
           _.-``__ “-._
      _.-``    `.  `_.  “-._         Redis 4.0.10 64 bit
  .-`` .-```.  ```\/    _.,_ “-._
 (    '      ,       .-`  | `,    )   Standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|   Port: 6379
 |    `-._   `._    /     _.-'    |   PID: 89887
  `-._    `-._  `-./  _.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |   http://redis.io
  `-._    `-._`-.__.-'_.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |
  `-._    `-._`-.__.-'_.-'    _.-'
      `-._    `-.__.-'    _.-'
          `-._        _.-'
              `-.__.-'
89887:M 11 Feb 20:17:55.323 # Server started, Redis version 3.0.7
89887:M 11 Feb 20:17:55.323 * The server is now ready to accept connections on port 6379
This output indicates that Redis is ready and listening in port 6379. You can open a new terminal window and execute the following command.
$ redis-cli
This is a shell client that connects to the Redis Server. You can subscribe to the "todos" topic by executing the following command.
127.0.0.1:6379> SUBSCRIBE todos
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "todos"
3) (integer) 1
Now you can run the project as usual (by running it inside your IDE or using Maven or Gradle). If you are using Maven, execute
$ ./mvnw spring-boot:run
After executing this command, you should have something similar to the following output in your logs.
...
Producer> Message Sent
Consumer> ToDo(id=null, description=workout tomorrow morning!, created=null, modified=null, completed=false)
ToDo created> ToDo(id=8a808087645bd67001645bd6785b0000, description=workout tomorrow morning!, created=2018-07-02T10:32:19.546, modified=2018-07-02T10:32:19.547, completed=false)
...
If you take a look at the Redis shell, you should see something like the following.
1) "message"
2) "todos"
3) "{\"id\":null,\"description\":\"workout tomorrow morning!\",\"created\":null,\"modified\":null,\"completed\":false}"

And of course, you can take a look in your browser at http://localhost:8080/toDos to see the new ToDo.

Well done! You have created a Spring Bot messaging app using Redis. You can shut down Redis by pressing Ctrl+C.
Remote Redis
If you want to access Redis remotely, you need to add the following properties to the application.properties file.
spring.redis.database=0
spring.redis.host=localhost
spring.redis.password=mysecurepassword
spring.redis.port=6379

You can always read about all the properties for Redis in the Spring Boot reference at https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html .

You saw that you need to use Redis as a messaging broker, but if you want to know more about the key-value store with Spring, you can check out the Spring Data Redis project at http://projects.spring.io/spring-data-redis/ .
WebSockets with Spring Boot

It might seem logical that a topic about WebSockets should be in the web chapter, but I consider WebSockets more related to messaging, and that’s why this section is in this chapter.

WebSockets is a new way of communication, replacing client/server web technology. It allows long-held single TCP socket connections between the client and server. It’s also called push technology, which is where the server can send data to the web without the client doing long polling to request a new change.

This section shows you an example where you send a message through a REST endpoint (Producer) and receive the messages (Consumer) using a webpage and JavaScript libraries.
ToDo App with WebSockets

Create the ToDo app that uses JPA REST Repositories. Every time there is a new ToDo, it is posted to a webpage. The connection from the webpage to the ToDo app uses WebSockets using the STOMP protocol.
Let’s start by opening your favorite browser and point to Spring Initializr. Add the following values to the fields.

    Group: com.apress.todo

    Artifact:todo-websocket

    Name: todo-websocket

    Package Name: com.apress.todo

    Dependencies: Websocket, Web, Lombok, JPA, REST Repositories, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 9-7).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig7_HTML.jpg
Figure 9-7

Spring Initializr
You can reuse and copy/paste the ToDo and ToDoRepository classes . Also you need to add the following dependencies; if you are using Maven, add the following to the pom.xml file.
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>sockjs-client</artifactId>
    <version>1.1.2</version>
</dependency>
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>stomp-websocket</artifactId>
    <version>2.3.3</version>
</dependency>
<!--  jQuery  -->
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>jquery</artifactId>
    <version>3.1.1</version>
</dependency>
<!-- Bootstrap -->
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>bootstrap</artifactId>
    <version>3.3.5</version>
</dependency>
If you are using Gradle, add the following dependencies to the build.gradle file .
compile('org.webjars:sockjs-client:1.1.2')
compile('org.webjars:stomp-websocket:2.3.3')
compile('org.webjars:jquery:3.1.1')
compile('org.webjars:bootstrap:3.3.5')

These dependencies create the web client that you need to connect to the messaging broker. The WebJars are a very convenient way to include external resources as packages, instead of worrying about downloading one by one.
ToDo Producer

The producer sends a STOMP message to a topic when a new ToDo is posted by using the HTTP POST method. To do this, it is necessary to catch the event that the Spring Data REST emits when the domain class is persisted to the database.
The Spring Data REST framework has several events that allow control before, during, and after a persistence action. Create a ToDoEventHandler class that is listening for the after-create event (see Listing 9-15).
package com.apress.todo.event;
import com.apress.todo.config.ToDoProperties;
import com.apress.todo.domain.ToDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
@Component
@RepositoryEventHandler(ToDo.class)
public class ToDoEventHandler {
    private Logger log = LoggerFactory.getLogger(ToDoEventHandler.class);
    private SimpMessagingTemplate simpMessagingTemplate;
    private ToDoProperties toDoProperties;
    public ToDoEventHandler(SimpMessagingTemplate simpMessagingTemplate,ToDoProperties toDoProperties){
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.toDoProperties = toDoProperties;
    }
    @HandleAfterCreate
    public void handleToDoSave(ToDo toDo){
        this.simpMessagingTemplate.convertAndSend(this.toDoProperties.getBroker() + "/new",toDo);
        log.info(">> Sending Message to WS: ws://todo/new - " + toDo);
    }
}
Listing 9-15

com.apress.todo.event.ToDoEventHandler.java
Listing 9-15 shows you the event handler that is receiving the after-create event. Let’s analyze it.

    @RepositoryEventHandler. This annotation tells the BeanPostProcessor that this class needs to be inspected for handler methods.

    SimpMessagingTemplate. This class is another implementation of the Template pattern and is used to send messages using the STOMP protocol. It behaves the same way as the other *Template classes from previous sections.

    ToDoProperties. This class is a custom properties handler. It describes the broker (todo.ws.broker), the endpoint (todo.ws.endpoint), and the application endpoint for WebSockets.

    @HandleAfterCreate. This annotation marks the method to get any event that happens after the domain class was persisted to the database. As you can see, it uses the ToDo instance that was saved into the database. In this method, you are using the SimpMessagingTemplate to send a ToDo instance to the /todo/new endpoint. Any subscriber to that endpoint gets the ToDo in JSON format (STOMP).

Next, let’s create the ToDoProperties class that hold the endpoints information (see Listing 9-16).
package com.apress.todo.config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "todo.ws")
public class ToDoProperties {
    private String app = "/todo-api-ws";
    private String broker = "/todo";
    private String endpoint = "/stomp";
}
Listing 9-16

com.apress.todo.config.ToDoProperties.java

The ToDoProperties class is a helper to hold information about the broker (/stomp) and where the web client connect to (topic - /todo/new).
Configuring the ToDo App

This time the ToDo app creates a messaging broker that accepts WebSocket communication and uses the STOMP protocol for message interchange.
Create the config class (see Listing 9-17).
package com.apress.todo.config;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
@Configuration
@EnableWebSocketMessageBroker
@EnableConfigurationProperties(ToDoProperties.class)
public class ToDoConfig implements WebSocketMessageBrokerConfigurer {
    private ToDoProperties props;
    public ToDoConfig(ToDoProperties props){
        this.props = props;
    }
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(props.getEndpoint()).setAllowedOrigins("*").withSockJS();
    }
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker(props.getBroker());
        config.setApplicationDestinationPrefixes(props.getApp());
    }
}
Listing 9-17

com.apress.todo.config.ToDoConfig.java
Listing 9-17 shows the ToDoConfig class. Let’s examine it.

    @Configuration. You know that this marks the class as configuration for the Spring container.

    @EnableWebSocketMessageBroker. This annotation uses auto-configuration to create all the necessary artifacts to enable broker-backed messaging over WebSockets using a very high-level messaging subprotocol. If you need to customize the endpoints, you need to override the methods from the WebSocketMessageBrokerConfigurer interface.

    WebSocketMessageBrokerConfigurer. It overrides methods to customize the protocols and endpoints.

    registerStompEndpoints(StompEndpointRegistry registry). This method registers the STOMP protocol; in this case, it registers the /stomp endpoint and uses the JavaScript library SockJS ( https://github.com/sockjs ).

    configureMessageBroker(MessageBrokerRegistry config). This method configures the message broker options. In this case, it enables the broker in the /todo endpoint. This means that the clients that want to use the WebSockets broker need to use the /todo to connect.

Next, let’s add information to the application.properties file .
# JPA
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=create-drop
# Rest Repositories
spring.data.rest.base-path=/api
# WebSocket
todo.ws.endpoint=/stomp
todo.ws.broker=/todo
todo.ws.app=/todo-api-ws

src/main/resources/application.properties

The application.properties file is declaring a new REST base-path endpoint (/api) because the client is an HTML page and it is the default index.html; this means that the REST Repositories live in the /api/* endpoint and not in the root of the app.
ToDo Web Client

The web client is the one making a connection to the messaging broker, where subscribe (using the STOMP protocol) receives any new ToDo that was posted. This client can be any type that handles WebSockets and knows the STOMP protocol.
Let’s create a simple index.html page that connects to the broker (see Listing 9-18).
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ToDo WebSockets</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
<div class="container theme-showcase" role="main">
    <div class="jumbotron">
        <h1>What ToDo?</h1>
        <p>An easy way to find out what your are going to do NEXT!</p>
    </div>
    <div class="page-header">
        <h1>Everybody ToDo's</h1>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">ToDo:</h3>
                </div>
                <div class="panel-body">
                    <div id="output">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/webjars/jquery/3.1.1/jquery.min.js"></script>
<script src="/webjars/sockjs-client/1.1.2/sockjs.min.js"></script>
<script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
<script>
    $(function(){
        var stompClient = null;
        var socket = new SockJS('http://localhost:8080/stomp');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/todo/new', function (data) {
                console.log('>>>>> ' + data);
                var json = JSON.parse(data.body);
                var result = "<span><strong>[" + json.created + "]</strong>&nbsp" + json.description + "</span><br/>";
                $("#output").append(result);
            });
        });
    });
</script>
</body>
</html>
Listing 9-18

src/main/resources/static/index.html

Listing 9-18 shows the index.html, the client that uses the SockJS class to connect to the /stomp endpoint. It subscribes to the /todo/new topic and waits until get a new ToDo is added to the list. The reference to the JavaScript libraries and the CSS is the WebJars class resource.
Running the ToDo App
Now you are ready to start your ToDo app. You can run the application as usual, either using your IDE or in a command line. If you are using Maven, execute
$ ./mvnw spring-boot:run
If you are using Gradle, execute
$ ./gradlew bootRun
Open a browser and go to http://localhost:8080. You should see an empty ToDo’s box. Next, open a terminal and execute the following commands.
$ curl -XPOST -d '{"description":"Learn to play Guitar"}' -H "Content-Type: application/json" http://localhost:8080/api/toDos
$ curl -XPOST -d '{"description":"read Spring Boot Messaging book from Apress"}' -H "Content-Type: application/json" http://localhost:8080/api/toDos
You can add more if you like. In your browser, you are seeing the ToDo’s (see Figure 9-8).
../images/340891_2_En_9_Chapter/340891_2_En_9_Fig8_HTML.jpg
Figure 9-8

SockJS and Stomp messages: ToDo’s List

Figure 9-8 shows the result of posting messages through WebSockets. Now imagine the possibilities for new applications that require notification in real time (such as creating real-time chatrooms, updating stocks on the fly for your customers, or updating your website without preview or restart). With Spring Boot and WebSockets, you are covered.
Note

All the code is available from the Apress site. You can also get the latest at https://github.com/felipeg48/pro-spring-boot-2nd .
Summary

This chapter discussed all the technologies that are used for messaging, including JMS and Artemis. It also discussed how to connect to a remote server by providing the server name and port in the application.properties file.

You learned about AMQP and RabbitMQ and how you can send and receive messages using Spring Boot. You also learned about Redis and how to use its Pub/Sub messaging. Finally, you learned about WebSockets and how easy it is to implement it with Spring Boot.

If you are into messaging, I wrote Spring Boot Messaging (Apress, 2017) ( www.apress.com/us/book/9781484212257 ), which talks about it in detail and exposes more messaging patterns, from simple application events to cloud solutions using Spring Cloud Stream and its transport abstraction.

The next chapter discusses the Spring Boot Actuator and how to monitor your Spring Boot application.