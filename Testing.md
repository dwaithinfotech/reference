Spring Testing Framework

One of the main ideas of the Spring Framework is to encourage developers to create simple and loosely coupled classes, program to interfaces, making the software more robust and extensible. The Spring Framework provides the tools for making unit and integration testing easy (actually, you don’t need Spring to test the functionality of your system if you really program interfaces); in other words, your application should be testable using the JUnit or TestNG test engines with objects (by simple instantiated using the new operator)—without Spring or any other container.

The Spring Framework has several testing packages that help create unit and integration testing for applications. It offers unit testing by providing several mock objects (Environment, PropertySource, JNDI, Servlet; Reactive: ServerHttpRequest and ServerHttpResponse test utilities) that help test your code in isolation.
One of the most commonly used testing features of the Spring Framework is integration testing. Its primary’s goals are

    managing the Spring IoC container caching between test execution

    transaction management

    dependency injection of test fixture instances

    Spring-specific base classes

The Spring Framework provides an easy way to do testing by integrating the ApplicationContext in the tests. The Spring testing module offers several ways to use the ApplicationContext , programmatically and through annotations:

    BootstrapWith. A class-level annotation to configure how the Spring TestContext Framework is bootstrapped.

    @ContextConfiguration. Defines class-level metadata to determine how to load and configure an ApplicationContext for integration tests. This is a must-have annotation for your classes, because that’s where the ApplicationContext loads all your bean definitions.

    @WebAppConfiguration. A class-level annotation to declare that the ApplicationContext loads for an integration test should be a WebApplicationContext.

    @ActiveProfile. A class-level annotation to declare which bean definition profile(s) should be active when loading an ApplicationContext for an integration test.

    @TestPropertySource. A class-level annotation to configure the locations of properties files and inline properties to be added to the set of PropertySources in the Environment for an ApplicationContext loaded for an integration test.

    @DirtiesContext. Indicates that the underlying Spring ApplicationContext has been dirtied during the execution of a test (modified or corrupted for example, by changing the state of a singleton bean) and should be closed.

There a lot more (@TestExecutionListeners, @Commit, @Rollback, @BeforeTransaction, @AfterTransaction, @Sql, @SqlGroup, @SqlConfig, @Timed, @Repeat, @IfProfileValue, and so forth).
As you can see, there are a lot of choices when you test with the Spring Framework. Normally, you always use the @RunWith annotation that wires up all the test framework goodies. For example,
@RunWith(SpringRunner.class)
@ContextConfiguration({"/app-config.xml", "/test-data-access-config.xml"})
@ActiveProfiles("dev")
@Transactional
public class ToDoRepositoryTests {
      @Test
      public void ToDoPersistenceTest(){
            //...
      }
}

Now, let’s see how the Spring Test Framework is used and all the features that Spring Boot offers.
Spring Boot Testing Framework

Spring Boot uses the power of the Spring Testing Framework by enhancing and adding new annotations and features that make testing easier for developers.

If you want to start using all the testing features by Spring Boot, you only need to add the spring-boot-starter-test dependency with scope test to your application. This dependency is already in place in the Spring Initializr service.

The spring-boot-starter-test dependency provides several test frameworks that play along very well with all the Spring Boot testing features: JUnit, AssertJ, Hamcrest, Mockito, JSONassert, and JsonPath. Of course, there are other test frameworks that play very nicely with the Spring Boot Test module; you only need to include those dependencies manually.

Spring Boot provides the @SpringBootTest annotation that simplifies the way you can test Spring apps. Normally, with Spring testing, you are required to add several annotations to test a particular feature or functionality of your app, but not in Spring Boot—although you are still required to use the @RunWith(SpringRunner.class) annotation to do your testing; if you do not, any new Spring Boot test annotation will be ignored. The @SpringBootTest has parameters that are useful when testing a web app, such as defining a RANDOM_PORT or DEFINED_PORT.
The following code snippet is the skeleton of what a Spring Boot test looks like.
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTests {
      @Test
      public void exampleTest() {
            ...
      }
}
Testing Web Endpoints
Spring Boot offers a way to test endpoints: a mocking environment called the MockMvc class .
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MockMvcToDoTests {
      @Autowired
      private MockMvc mvc;
      @Test
      public void toDoTest() throws Exception {
          this.mvc
          .perform(get("/todo"))
          .andExpect(status().isOk())
          .andExpect(content()
             .contentType(MediaType.APPLICATION_JSON_UTF8));
      }
}
You can also use the TestRestTemplate class.
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ToDoSimpleRestTemplateTests {
    @Autowired
    private TestRestTemplate restTemplate;
    @Test
    public void toDoTest() {
        String body = this.restTemplate.getForObject("/todo", String.class);
        assertThat(body).contains("Read a Book");
    }
}

This code shows a test that is running a full server and using the TestRestTemplate instance to do a call to the /todo endpoint. Here we are assuming a String return. (This is not the best way to test a JSON return. Don’t worry, you see the proper way to use the TestRestTemplate class later on).
Mocking Beans
The Spring Boot testing module offers a @MockBean annotation that defines a Mockito mock for a bean inside the ApplicationContext. In other words, you can mock a new Spring bean or replace an existing definition by adding this annotation. Remember, this is happening inside the ApplicationContext.
@RunWith(SpringRunner.class)
@SpringBootTest
public class ToDoSimpleMockBeanTests {
    @MockBean
    private ToDoRepository repository;
    @Test
    public void toDoTest() {
        given(this.repository.findById("my-id"))
            .Return(new ToDo("Read a Book"));
        assertThat(
            this.repository.findById("my-id").getDescription())
            .isEqualTo("Read a Book");
    }
}
Spring Boot Testing Slices

One of the most important features that Spring Boot offers is a way to execute test without the need for a certain infrastructure. The Spring Boot testing module includes slices to test specific parts of the application without the need of a server or a database engine.
@JsonTest
The Spring Boot testing module provides the @JsonTest annotation , which helps with object JSON serialization and deserialization, and verifies that everything works as expected. @JsonTest auto-configures the supported JSON mapper, depending of which library is in the classpath: Jackson, GSON, or JSONB.
@RunWith(SpringRunner.class)
@JsonTest
public class ToDoJsonTests {
    @Autowired
    private JacksonTester<ToDo> json;
    @Test
    public void toDoSerializeTest() throws Exception {
        ToDo toDo = new ToDo("Read a Book");
        assertThat(this.json.write(toDo))
        .isEqualToJson("todo.json");
        assertThat(this.json.write(toDo))
        .hasJsonPathStringValue("@.description");
        assertThat(this.json.write(toDo))
        .extractingJsonPathStringValue("@.description")
        .isEqualTo("Read a Book");
    }
    @Test
    public void toDoDeserializeTest() throws Exception {
        String content = "{\"description\":\"Read a Book\",\"completed\": true }";
        assertThat(this.json.parse(content))
                .isEqualTo(new ToDo("Read a Book", true));
         assertThat(
           this.json.parseObject(content).getDescription())
         .isEqualTo("Read a Book");
    }
}
@WebMvcTest

If you need to test your controllers without using a complete server, Spring Boot provides the @WebMvcTest annotation that auto-configures the Spring MVC infrastructure and limits scanned beans to @Controller, @ControllerAdvice, @JsonComponent, Converter, GenericConverter, Filter, WebMvcConfigurer, and HandlerMethodArgumentResolver; so you know if your controllers are working as expected.
It’s important to know that beans marked as @Component are not scanned when using this annotation, but you can still use the @MockBean if needed.
@RunWith(SpringRunner.class)
@WebMvcTest(ToDoController.class)
public class ToDoWebMvcTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ToDoRepository toDoRepository;
    @Test
    public void toDoControllerTest() throws Exception {
        given(this.toDoRepository.findById("my-id"))
                .Return(new ToDo("Do Homework", true));
        this.mvc.perform(get("/todo/my-id").accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk()).andExpect(content().string("{\"id\":\"my-id\",\"description\":\"Do Homework\",\"completed\":true}"));
    }
}
@WebFluxTest

Spring Boot provides the @WebFluxTest annotation for any reactive controller. This annotation auto-configures the Spring WebFlux module infrastructure and scans only for @Controller, @ControllerAdvice, @JsonComponent, Converter, GenericConverter, and WebFluxConfigurer.
It’s important to know that beans marked as @Component are not scanned when using this annotation, but you can still use the @MockBean if needed.
@RunWith(SpringRunner.class)
@WebFluxTest(ToDoFluxController.class)
public class ToDoWebFluxTest {
    @Autowired
    private WebTestClient webClient;
    @MockBean
    private ToDoRepository toDoRepository;
    @Test
    public void testExample() throws Exception {
        given(this.toDoRepository.findAll())
                .Return(Arrays.asList(new ToDo("Read a Book"), new ToDo("Buy Milk")));
        this.webClient.get().uri("/todo-flux").accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody(List.class);
    }
}
@DataJpaTest
If you need to test your JPA application, the Spring Boot testing module offers @DataJpaTest, which auto-configures in-memory embedded databases. It scans @Entity. This annotation won’t load any @Component bean. It also provides the TestEntityManager helper class that is very similar to the JPA EntityManager class, which specializes in testsing.
@RunWith(SpringRunner.class)
@DataJpaTest
public class TodoDataJpaTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private ToDoRepository repository;
    @Test
    public void toDoDataTest() throws Exception {
        this.entityManager.persist(new ToDo("Read a Book"));
        Iterable<ToDo> toDos = this.repository.findByDescriptionContains("Read a Book");
        assertThat(toDos.iterator().next()).toString().contains("Read a Book");
    }
}
Remember that using @DataJpaTest uses embedded in-memory database engines (H2, Derby, HSQL), but if you want to test with a real database, you need to add the following @AutoConfigureTestDatabase(replace=Replace.NONE) annotation as a marker for the test class.
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class TodoDataJpaTests {
      //...
}
@JdbcTest
This annotation is very similar to @DataJpaTest; the only difference is that it does pure JDBC-related tests. It auto-configures the in-memory embedded database engine and it configures the JdbcTemplate class. It omits every class marked as @Component.
@RunWith(SpringRunner.class)
@JdbcTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TodoJdbcTests {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    private CommonRepository<ToDo> repository;
      @Test
      public void toDoJdbcTest() {
          ToDo toDo = new ToDo("Read a Book");
        this.repository = new ToDoRepository(jdbcTemplate);
        this.repository.save(toDo);
        ToDo result = this.repository.findById(toDo.getId());
        assertThat(result.getId()).isEqualTo(toDo.getId());
      }
}
@DataMongoTest
The Spring Boot testing module provides the @DataMongoTest annotation to test Mongo applications. It auto-configures the in-memory embedded Mongo server (if available), if it doesn’t, you need to add the right spring.data.mongodb.* properties. It configures the MongoTemplate class and it scans for @Document annotations. @Component beans aren’t scanned.
@RunWith(SpringRunner.class)
@DataMongoTest
public class TodoMongoTests {
    @Autowired
    private MongoTemplate mongoTemplate;
      @Test
      public void toDoMongoTest() {
        ToDo toDo = new ToDo("Read a Book");
        this.mongoTemplate.save(toDo);
        ToDo result = this.mongoTemplate.findById(toDo.getId(),ToDo.class);
        assertThat(result.getId()).isEqualTo(toDo.getId());
      }
}
If you require an external MongoDB server (not in-memory embedded), add the excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class parameter to the @DataMongoTest annotation.
@RunWith(SpringRunner.class)
@DataMongoTest(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
public class ToDoMongoTests {
      // ...
}
@RestClientTest
Another important annotation is the @RestClientTest, which tests your REST clients. This annotation auto-configures Jackson, GSON, and JSONB support. It configures the RestTemplateBuilder and adds support for MockRestServiceServer.
@RunWith(SpringRunner.class)
@RestClientTest(ToDoService.class)
public class ToDoRestClientTests {
    @Autowired
    private ToDoService service;
    @Autowired
    private MockRestServiceServer server;
    @Test
    public void toDoRestClientTest()
            throws Exception {
        String content = "{\"description\":\"Read a Book\",\"completed\": true }";
        this.server.expect(requestTo("/todo/my-id"))
                .andRespond(withSuccess(content,MediaType.APPLICATION_JSON_UTF8));
        ToDo result = this.service.findById("my-id");
        assertThat(result).isNotNull();
        assertThat(result.getDescription()).contains("Read a Book");
    }
}

There are many other slices that you can check out. The important note to take is that you aren’t required to have a whole infrastructure or to have servers running to do testing. Slices facilitate more testing for Spring Boot apps.