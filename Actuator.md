This chapter discusses the Spring Boot Actuator module and explains how you can use all its features to monitor your Spring Boot applications.

A common task during and after development that every developer does is to start checking out the logs. Developers check to see if the business logic works as it supposed to, or checks out the processing time of services, and so on. Even though they should have their unit, integration, and regression tests in place, they are not exempt from external failures, including the network (connections, speed, etc.), disk (space, permissions, etc.), and more.

When you deploy to production, it’s even more critical. You must pay attention to your applications and sometimes to the whole system. When you start depending on non-functional requirements, such as monitoring systems that check the health of the different applications, or maybe that sets alerts when your application gets to a certain threshold, or worse, when your application crashes, you need to act ASAP.

Developers depend on many third-party technologies to do their job, and I’m not saying that this is bad, but this means that all the heavy lifting is by the DevOps teams. They must monitor every single application and the entire system as a whole.
Spring Boot Actuator

Spring Boot includes an Actuator module, which introduces production-ready non-functional requirements to your application. The Spring Boot Actuator module provides monitoring, metrics, and auditing—right out of the box.

What makes the Actuator module more attractive is that you can expose data through different technologies, such as HTTP (endpoints) and JMX. Spring Boot Actuator metrics monitoring can be done using the Micrometer Framework ( http://micrometer.io/ ), that allows you to write once your metrics code and use it in any vendor-neutral engine, such as Prometheus, Atlas, CloudWatch, Datadog, and many, many more.
ToDo App with Actuator
Let’s start using the Spring Boot Actuator module in the ToDo application to see how Actuator works. You can start from scratch or you can follow along in the next sections to learn what you need to do. If you are starting from scratch, then you can go to Spring Initializr ( https://start.spring.io ) and add the following values to the fields.

    Group: com.apress.todo

    Artifact: todo-actuator

    Name: todo-actuator

    Package Name: com.apress.todo

    Dependencies: Web, Lombok, JPA, REST Repositories, Actuator, H2, MySQL

You can select either Maven or Gradle as the project type. Then you can press the Generate Project button, which downloads a ZIP file. Uncompress it and import the project in your favorite IDE (see Figure 10-1).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig1_HTML.jpg
Figure 10-1

Spring Initializr
There is nothing from other projects right now; the only new dependency is the Actuator module. You can copy/reuse the ToDo domain class and the ToDoRepository interface (see Listings 10-1 and 10-2).
package com.apress.todo.domain;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
@Entity
@Data
public class ToDo {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @NotNull
    @NotBlank
    private String description;
    @Column(insertable = true, updatable = false)
    private LocalDateTime created;
    private LocalDateTime modified;
    private boolean completed;
    public ToDo(){}
    public ToDo(String description){
        this.description = description;
    }
    @PrePersist
    void onCreate() {
        this.setCreated(LocalDateTime.now());
        this.setModified(LocalDateTime.now());
    }
    @PreUpdate
    void onUpdate() {
        this.setModified(LocalDateTime.now()) ;
    }
}
Listing 10-1

com.apress.todo.domain.ToDo.java
package com.apress.todo.repository;
import com.apress.todo.domain.ToDo;
import org.springframework.data.repository.CrudRepository;
public interface ToDoRepository extends CrudRepository<ToDo,String> { }
Listing 10-2

com.apress.todo.repository.ToDoRepository.java

Before running the ToDo app, take a look that you have the spring-boot-starter-actuator dependency in your pom.xml (if you are using Maven) or build.gradle (if you are using Gradle).
You can run the ToDo app, and the important thing to notice is the logs output. You should have something similar.
INFO 41925 --- [main] s... : Mapped "{[/actuator/health],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}"  ...
INFO 41925 --- [main] s... : Mapped "{[/actuator/info],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}"  ...
INFO 41925 --- [main] s... : Mapped "{[/actuator],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}"  ...
By default, the Actuator module exposes three endpoints that you can visit.

    /actuator/health. This endpoint provides basic application health information. If access is from the browser or with a command line, you get the following response:
    {
        "status": "UP"
    }

    /actuator/info. This endpoint displays arbitrary application information. If you access this endpoint, you get an empty response; but if you add the following to your application.properties file:
    spring.application.name=todo-actuator
    info.application-name=${spring.application.name}
    info.developer.name=Awesome Developer
    info.developer.email=awesome@example.com

    You get the following:
    {
        "application-name": "todo-actuator",
        "developer": {
            "name": "Awesome Developer",
            "email": "awesome@example.com"
        }
    }

    /actuator. This endpoint is the prefix of all the actuator endpoints. If you go to this endpoint through a browser or command line, you get this:
    {
      "_links": {
        "self": {
            "href": "http://localhost:8080/actuator",
            "templated": false
        },
        "health": {
            "href": "http://localhost:8080/actuator/health",
            "templated": false
        },
        "info": {
            "href": "http://localhost:8080/actuator/info",
            "templated": false
        }
      }
    }

By default, all the endpoints (there are more) are enabled, except for the /actuator/shutdown endpoint; but why are only two endpoints exposed (health and information)? Actually, all of them are exposed through JMX, and this is because some of them contain sensitive information; so, it’s important to know what information to expose through the web.

If you want to expose them over the web, there are two properties: management.endpoints.web.exposure.include and management.endpoints.web.exposure.exclude. You can list them individually separated by a comma or include all of them by using the *.

The same applies for exposing the endpoints through JMX with the properties.

management.endpoints.jmx.exposure.include and management.endpoints.jmx.exposure.exclude. Remember that by default all the endpoints are exposed through JMX.
As I mentioned before, you not only have a way to expose the endpoints but also to enable them. You can use the following semantic: management.endpoint.<ENDPOINT-NAME>.enabled. So, if you want to enable the /actuator/shutdown (it is disabled by default) you need to do this in application.properties.
management.endpoint.shutdown.enabled=true
You can add the following property to your application.properties file to expose all the web actuator endpoints.
management.endpoints.web.exposure.include=*

If you take a look at the output, you get more actuator endpoints, like /actuator/beans, /actuator/conditions, and so forth. Let’s review some of them in more detail.
/actuator
The /actuator endpoint is the prefix of all the endpoints, but if you access it, it provides a hypermedia-based discovery page for all the other endpoints. So, if you go to http://localhost:8080/actuator, you should see something similar to Figure 10-2.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig2_HTML.jpg
Figure 10-2

http://localhost:8080/actuator
/actuator/conditions
This endpoint displays the auto-configuration report. It gives you two groups: positiveMatches and negativeMatches. Remember that the main feature of Spring Boot is that it auto-configures your application by seeing the classpath and dependencies. This has everything to do with the starter poms and extra dependencies that you add to your pom.xml file. If you go to http://localhost:8080/actuator/conditions, you should see something similar to Figure 10-3.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig3_HTML.jpg
Figure 10-3

http://localhost:8080/actuator/conditions
/actuator/beans
This endpoint displays all the Spring beans that are used in your application. Remember that even though you add a few lines of code to create a simple web application, behind the scenes, Spring starts to create all the necessary beans to run your app. If you go to http://localhost:8080/actuator/beans, you should see something similar to Figure 10-4.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig4_HTML.jpg
Figure 10-4

http://localhost:8080/actuator/beans
/actuator/configprops
This endpoint lists all the configuration properties that are defined by the @ConfigurationProperties beans, which is something that I showed you in earlier chapters. Remember, you can add your own configuration properties prefixes and they can be defined and accessed in the application.properties or YAML files. Figure 10-5 shows an example of this endpoint.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig5_HTML.jpg
Figure 10-5

http://localhost:8080/actuator/configprops
/actuator/threaddump
This endpoint performs a thread dump of your application. It shows all the threads running and their stack trace of the JVM that is running your app. Go to http://localhost:8080/actuator/threaddump endpoint (see Figure 10-6).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig6_HTML.jpg
Figure 10-6

http://localhost:8080/actuator/threaddump
/actuator/env
This endpoint exposes all the properties from Spring’s ConfigurableEnvironment interface. This shows any active profiles and system environment variables and all application properties, including the Spring Boot properties. Go to http://localhost:8080/actuator/env (see Figure 10-7).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig7_HTML.jpg
Figure 10-7

http://localhost:8080/actuator/env
/actuator/health
This endpoint shows the health of the application. By default, it shows you the overall system health.
{
   "status": "UP"
}
If you want to see more information about other systems, you need to use the following property in the application.properties file.
management.endpoint.health.show-details=always
Modify the application.properties and re-run the ToDo app. If you have a database app (we are), you see the database status, and by default, you also see the diskSpace from your system. If you are running your app, you can go to http://localhost:8080/actuator/health (see Figure 10-8).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig8_HTML.jpg
Figure 10-8

http://localhost:8080/actuator/health - with details
/actuator/info

This endpoint displays the public application information. This means that you need to add this information to application.properties. It’s recommended that you add it if you have multiple Spring Boot applications.
/actuator/loggers
This endpoint displays all the loggers available in your app. Figure 10-9 shows the level for a specific package.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig9_HTML.jpg
Figure 10-9

http://localhost:8080/actuator/loggers
/actuator/loggers/{name}
With this endpoint, you can look for a particular package and its log level. So, if you configure, for example, logging.level.com.apress.todo=DEBUG and you reach the http://localhost:8080/actuator/loggers/com.apress.todo endpoint, you get the following.
{
  "configuredLevel": DEBUG,
  "effectiveLevel": "DEBUG"
}
/actuator/metrics
This endpoint shows the metrics information of the current application, where you can determine the how much memory it’s using, how much memory is free, the uptime of your application, the size of the heap being used, the number of threads used, and so on (see Figure 10-10 and Figure 10-11).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig10_HTML.jpg
Figure 10-10

http://localhost:8080/actuator/metrics
You can access every metric by adding the name at the end of the endpoint; so if you want to know more about jvm.memory.max, you need to reach http://localhost:8080/actuator/metrics/jvm.memory.max (see Figure 10-11).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig11_HTML.jpg
Figure 10-11

http://localhost:8080//actuator/metrics/jvm.memory.max

If you take a look at Figure 10-11, in the availableTags section, you can get more information by appending tag=KEY:VALUE. You can use http://localhost:8080/actuator/metrics/jvm.memory.max?tag=area:heap and get information about the heap.
/actuator/mappings
This endpoint shows all the lists of all the @RequestMapping paths declared in your application. This is very useful if you want to know more about which mappings are declared. If your application is running, you can go to the http://localhost:8080/actuator/mappings endpoint (see Figure 10-12).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig12_HTML.jpg
Figure 10-12

http://localhost:8080/actuator/mappings
/actuator/shutdown
This endpoint is not enabled by default. It allows the application to be gracefully shut down . This endpoint is sensitive, which means that it can be used with security, and it should be. If your application is running, you can stop it now. If you want to enable the /actuator/shutdown endpoint, you need to add the following to the application.properties.
management.endpoint.shutdown.enabled=true
It’s wise to have this endpoint secured. You’d need to add the spring-boot-starter-security dependency to your pom.xml (if you are using Maven).
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
If you are using Gradle, you can add the following dependency to your build.gradle.
complie ('org.springframework.boot: spring-boot-starter-security')

Remember that by adding the security dependency, you enable security by default. The username is user and the password is printed in the logs. Also, you can establish better security by using in-memory, database, or LDAP users; see the Spring Boot security chapter for more information.
For now, let’s add management.endpoint.shutdown.enabled=true and the spring-boot-starter-security dependency and rerun the application. After running the application, take a look at the logs and save the password that is printed so that it can be used with the /actuator/shutdown endpoint.
...
Using default security password: 2875411a-e609-4890-9aa0-22f90b4e0a11
...
Now if you open a terminal, you can execute the following command.
$ curl -i -X POST http://localhost:8080/shutdown -u user:2875411a-e609-4890-9aa0-22f90b4e0a11
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: 0
X-Frame-Options: DENY
Strict-Transport-Security: max-age=31536000 ; includeSubDomains
X-Application-Context: application
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Wed, 17 Feb 2018 04:22:58 GMT
{"message":"Shutting down, bye..."}

As you can see from this output, you are using a POST method to access the /actuator/shutdown endpoint, and you are passing the user and the password that was printed before. The result is the Shutting down, bye.. message. And of course, your application is terminated. Again, it’s important to know that this particular endpoint must be secured at all times .
/actuator/httptrace
This endpoint shows the trace information, which are normally the last few HTTP requests. This endpoint can be useful to see all the request information and the information returned to debug your application at the HTTP level. You can run your application and go to http://localhost:8080/actuator/httptrace. You should see something similar to Figure 10-13.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig13_HTML.jpg
Figure 10-13

http://localhost:8080/actuator/httptrace
Changing the Endpoint ID

You can configure the endpoint ID, which changes the name. Imagine that you don’t like the /actuator/beans endpoint, which is referring to Spring beans, so what about if you change this endpoint to /actuator/spring.

You make this change in the application.properties file in the form of management.endpoints.web.path-mapping.<endpoint-name>=<new-name>; for example, management.endpoints.web.path-mapping.beans=spring.

If you re-run your application (stop and restart to apply the changes), you can access the /actuator/beans endpoint using the /actuator/spring endpoint instead.
Actuator CORS Support

With the Spring Boot Actuator module, you can configure CORS (cross-origin resource sharing), which allows you to specify which cross domains are authorized to use the actuator’s endpoints. Normally, this allows interapplications connect to your endpoints, and due to security reasons, only the authorized domains are able to execute these endpoints.
You configure this in the application.properties file.
management.endpoints.web.cors.allowed-origins=http://mydomain.com
management.endpoints.web.cors.allowed-methods=GET, POST

If your application is running, stop it and re-run it.

Normally in the management.endpoints.web.cors.allowed-origins, you should put a domain name like http://mydomain.com or maybe http://localhost:9090 (not the *), which allows access to your endpoints to avoid any hack to your site. This would be very similar to using the @CrossOrigin(origins = "http://localhost:9000") annotation in any controller.
Changing the Management Endpoints Path
By default, the Spring Boot Actuator has its management in /actuator as the root, which means that all the actuator’s endpoints can be accessed from /actuator; for example, /actuator/beans, /actuator/health, and so on. Before you continue, stop your application. You can change its management context path by adding the following property to the application.properties file.
management.endpoints.web.base-path=/monitor

If you re-run your application, you see that EndpointHandlerMapping is mapping all the endpoints by adding the /monitor/<endpoint-name> context path. You can now access the /httptrace endpoint through http://localhost:8080/monitor/httptrace.
You can also change the server address, add SSL, use a particular IP, or change the port for the endpoints, with the management.server.* properties.
management.server.servlet.context-path=/admin
management.server.port=8081
management.server.address=127.0.0.1

This configuration has its endpoint with the context-path /admin/actuator/<endpoint-name>. The port is 8081 (this means that you have two ports listening: 8080 for your application and 8081 for your management endpoints). The endpoints or management is bound to the 127.0.0.1 address.

If you want to disable the endpoints (for security reasons), you have two options, you can use management.endpoints.enabled-by-default=false or you can use the management.server.port=-1 properties.
Securing Endpoints
You can secure your actuator endpoints by including spring-boot-starter-security and configuring WebSecurityConfigurerAdapter ; this is through the HttpSecurity and RequestMatcher configurations.
@Configuration
public class ToDoActuatorSecurity extends WebSecurityConfigurerAdapter {
      @Override
      protected void configure(HttpSecurity http) throws Exception {
            http
            .requestMatcher(EndpointRequest.toAnyEndpoint())
            .authorizeRequests()
            .anyRequest().hasRole("ENDPOINT_ADMIN")
            .and()
            .httpBasic();
      }
}

It is important to know that the ENDPOINT_ADMIN role is required to access the endpoints for security.
Configuring Endpoints
By default, you see that actuator endpoints cache responds to read operations that don’t accept any parameters; so if you need to change this behavior, you can use the management.endpoint.<endpoint-name>.cache.time-to-live property. And as another example, if you need to change the /actuator/beans cache, you can add the following to the application.properties file.
management.endpoint.beans.cache.time-to-live=10s
Implementing Custom Actuator Endpoints

You can extend or create a custom actuator endpoint. You need to mark your class with @Endpoint and also mark your methods with @ReadOperation, @WriteOperation or @DeleteOperation; by default, your endpoint is exposed over JMX and through the web over HTTP.

You can be more specific and decide if you only want to expose your endpoint to JMX, and then mark your class as @JmxEndpoint. If you only need it for the web, then you mark your class with @WebEndpoint.

When creating the methods, you can accept parameters, which are converted to the right type needed by the ApplicationConversionService instance. These types consume the application/vnd.spring-boot.actuator.v2+json and application/json content type.

You can return any type (even void or Void) in any method signature. Normally, the returned content type varies depending on the type. If it is an org.springframework.core.io.Resource type, it returns an application/octet-stream content type; for all other types, it returns an application/vnd.spring-boot.actuator.v2+json, application/json content type.

When using your custom actuator endpoint over the web, the operations have their own HTTP method defined: @ReadOperation (Http.GET), @WriteOperation (Http.POST) and @DeleteOperation (Http.DELETE).
ToDo App with Custom Actuator Endpoints

Let’s create a custom endpoint (/todo-stats) that shows the count of ToDo’s in the database and how many are completed. Also, we can create a write operation that can complete a ToDo and even an operation for removing a ToDo.
Let’s create ToDoStatsEndpoint to hold all the logic of the custom endpoint (see Listing 10-3) .
package com.apress.todo.actuator;
import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.ToDoRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.actuate.endpoint.annotation.*;
import org.springframework.stereotype.Component;
@Component
@Endpoint(id="todo-stats")
public class ToDoStatsEndpoint {
    private ToDoRepository toDoRepository;
    ToDoStatsEndpoint(ToDoRepository toDoRepository){
        this.toDoRepository = toDoRepository;
    }
    @ReadOperation
    public Stats stats() {
        return new Stats(this.toDoRepository.count(),this.toDoRepository.countByCompleted(true));
    }
    @ReadOperation
    public ToDo getToDo(@Selector String id) {
        return this.toDoRepository.findById(id).orElse(null);
    }
    @WriteOperation
    public Operation completeToDo(@Selector String id) {
        ToDo toDo = this.toDoRepository.findById(id).orElse(null);
        if(null != toDo){
            toDo.setCompleted(true);
            this.toDoRepository.save(toDo);
            return new Operation("COMPLETED",true);
        }
        return new Operation("COMPLETED",false);
    }
    @DeleteOperation
    public Operation removeToDo(@Selector String id) {
        try {
            this.toDoRepository.deleteById(id);
            return new Operation("DELETED",true);
        }catch(Exception ex){
            return new Operation("DELETED",false);
        }
    }
    @AllArgsConstructor
    @Data
    public class Stats {
        private long count;
        private long completed;
    }
    @AllArgsConstructor
    @Data
    public class Operation{
        private String name;
        private boolean successful;
    }
}
Listing 10-3

com.apress.todo.actuator.ToDoStatsEndpoint.java
Listing 10-3 shows the custom endpoint that does operations such as showing stats (the total number of ToDo’s and the number that are completed). It gets a ToDo object, removes it, and sets it as completed. Let’s review it.

    @Endpoint. Identifies a type as being an actuator endpoint that provides information about the running application. Endpoints can be exposed over a variety of technologies, including JMX and HTTP. This is the ToDoStatsEndpoint class that is your endpoint for the actuator.

    @ReadOperation. Identifies a method on an endpoint as being a read operation (sees that this class returns the ToDo by ID).

    @Selector. A selector can be used on a parameter of an endpoint method to indicate that the parameter selects a subset of the endpoint’s data. This is a way to modify a value, in this case, for updating the ToDo as completed.

    @WriteOperation. Identifies a method on an endpoint as being a write operation. This is very similar to a POST event.

    @DeleteOperation. Identifies a method on an endpoint as being a delete operation.

Listing 10-3 is very similar to a REST API, but in this case, all of these methods are exposed through a JMX protocol. Also, the stats method is using toDoRepository to call the countByCompleted method. Let’s add it to the ToDoRepository interface (see Listing 10-4).
package com.apress.todo.repository;
import com.apress.todo.domain.ToDo;
import org.springframework.data.repository.CrudRepository;
public interface ToDoRepository extends CrudRepository<ToDo,String> {
    public long countByCompleted(boolean completed);
}
Listing 10-4

com.apress.todo.repository.ToDoRepository.java – v2

Listing 10-4 shows version 2 of the ToDoRepository interface. This interface now has a new method declaration, countByCompleted; remember that this is a named query method and the Spring Data module takes care of creating the proper SQL statement to count the number of ToDo’s that are already completed.
As you can see, this is very straightforward for creating a custom endpoint. Now, if you run the application, and go to http://localhost:8080/actuator, you should see the todo-stats endpoint listed (see Figure 10-14).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig14_HTML.jpg
Figure 10-14

http://localhost:8080/actuator - todo-stats custom endpoint
If you click the first todo-stats link, you see what’s shown in Figure 10-15.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig15_HTML.jpg
Figure 10-15

http://localhost:8080/actuator/todo-stats
Very easy right? But what about the other operations. Let’s try them out. For this we are going to use JMX with JConsole (it comes with the JDK installation). You can open a terminal window and execute the jconsole command.

    1.
    Select from the com.apress.todo.ToDoActuatorApplication list and click Connect.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig16_HTML.jpg
     
    2.
    Right now there is no secured connection, but it’s OK to click the Insecure Connection button.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig17_HTML.jpg
     
    3.
    From the main screen, select the MBeans tab. Expand the org.springframework.boot package and the Endpoint folder. You see the Todo-stats. You can expand it and see all the operations.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig18_HTML.jpg
     
    4.
    Click the stats item to see the MBeans operation stats.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig19_HTML.jpg
     
    5.
    You can click the stats button (that actually is the call to the stats method), and you will get.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig20_HTML.jpg
     

As you can see, this is the same as going through the web. You can experiment with the completeToDo operation.

    6.
    Click the completeToDo operation. On the right, fill out the ID field with ebcf1850563c4de3b56813a52a95e930, which is the Buy Movie Tickets ToDo that is not completed.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig21_HTML.jpg
     
    7.
    Click completeToDo to get the confirmation (an Operation object).
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig22_HTML.jpg
     
    8.
    If you redo the stats operation, you should now see that two are completed.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig23_HTML.jpg
     

As you can see, it is very easy to use JMX with the JConsole tool. You now know how to create a custom endpoint for the data that you need.
Spring Boot Actuator Health

Nowadays, we are looking for visibility in our systems, meaning that we need to monitor them closely and react to any event. I remember a long time ago, the way to monitor a server was with a simple ping; but now that’s not enough. We not only monitor servers but systems and their insights. We are still required to see if our system is up, and if not, we need to get more information about the problem.
Spring Boot Actuator health endpoint to the rescue! The /actuator/health endpoint provides the status, or a health check, of your running application. It provides a particular property, management.endpoint.health.show-details, which you can use to show more information about the entire system. The following are the possible values.

    never. The details are never shown; this is the default.

    when-authorized. Details are shown only to authorized users; you can configure the roles by setting the management.endpoint.health.roles property.

    always. All the details are shown to all users.

Spring Boot Actuator offers the HealthIndicator interface that collects all information about the system; it returns a Health instance that contains all of this information. Actuator Health has several out-of-the-box health indicators that are auto-configured using a health aggregator to determine the final status of the system. It is very similar to a logging level. You can up and running. Don’t’ worry. I am going to show this with an example.
The following are some of the health indicators that are auto-configured.

    CassandraHealthIndicator . Checks that the Cassandra database is up and running.

    DiskSpaceHealthIndicator . Checks for low disk space.

    RabbitHealthIndicator . Checks that he Rabbit server is up and running.

    RedisHealthIndicator . Checks that the Redis server is up and running.

    DataSourceHealthIndicator . Checks for a database connection from the data source.

    MongoHealthIndicator . Checks that the MongoDB is up and running.

    MailHealthIndicator . Checks that the mail server is up.

    SolrHealthIndicator . Checks that the Solr server is up.

    JmsHealthIndicator . Checks that the JMS broker is up and running.

    ElasticsearchHealthIndicator . Checks that the ElasticSearch cluster is up.

    Neo4jHealthIndicator . Checks that the Neo4j sever is up and running.

    InfluxDBHealthIndicator . Checks that the InfluxDB server is up.

There are many more. All of these are auto-configured if the dependency is in your classpath; in other words, you don’t need to worry about configuring or using them.
Let’s test the health indicator.

    1.

    Make sure that you have (in the ToDo app) the management.endpoints.web.exposure.include=* in the application.properties file.
     
    2.

    Add the management.endpoint.health.show-details=always property to the application.properties file.
     
    3.

    If you run the ToDo app, and access http://localhost:8080/actuator/health, you should get the following.
     

../images/340891_2_En_10_Chapter/340891_2_En_10_Fig24_HTML.jpg
The H2 database DataSourceHealthIndicator and DiskSpaceHealthIndicator are being auto-configured.

    4.
    Add the following dependency to your pom.xml file (if you are using Maven).
    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-amqp</artifactId>
    </dependency>

    If you are using Gradle, add the following dependency to your build.gradle file.
    compile('org.springframework.boot:spring-boot-starter-amqp')
     

    5.
    You guessed right. We are adding AMQP dependencies. Re-run the app and take a look at the /actuator/health endpoint .
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig25_HTML.jpg
     

Because you added the spring-boot-starter-amqp dependency, it is about the RabbitMQ broker, and you have the actuator, RabbitHealthIndicator is auto-configured to reach for a local host (or a specific broker with spring.rabbitmq.* properties settings). If it is alive, then it reports it. In this case, you see that some failed to connect in the logs, and in the health endpoint, you see that the system is down. If you have a RabbitMQ broker (from the previous chapter), you can run it (with the rabbitmq-server command) and refresh the health endpoint . You see that everything is up!
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig26_HTML.jpg

That’s it. This is how you can use all the out-of-the-box health indicators. Add the required dependency—and you got it!
ToDo App with Custom HealthIndicator

Now it’s the ToDo’s app turn to have its own custom health indicator. It’s very easy to implement one. You need to implement the HealthIndicator interface and return the desired state with a Health instance.
Create ToDoHealthCheck to visit a FileSystem path and perform a check up to see if it is available, readable, and writeable (see Listing 10-5).
package com.apress.todo.actuator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;
import java.io.File;
@Component
public class ToDoHealthCheck implements HealthIndicator {
    private String path;
    public ToDoHealthCheck(@Value("${todo.path:/tmp}")String path){
        this.path = path;
    }
    @Override
    public Health health() {
        try {
            File file = new File(path);
            if(file.exists()){
                if(file.canWrite())
                    return Health.up().build();
                return Health.down().build();
            }else{
                return Health.outOfService().build();
            }
        }catch(Exception ex) {
            return Health.down(ex).build();
        }
    }
}
Listing 10-5

com.apress.todo.actuator.ToDoHealthCheck.java

Listing 10-5 shows the ToDoHealthCheck class that is marked as @Component and is the implementation of the HealthIndicator interface. It is necessary to implement the health method (see that the Health class has a fluent API that helps create the status of the health. Analyze the code and see what the path variable must be set as (a property in the environment, an argument in the command line, or in application.properties); otherwise, use /tmp by default. If this path exists, then it checks if you can write it; if so, it exposes the UP status, and if not, it reports a DOWN status. If the path doesn’t exist, it reports an OUT_OF_SERVICE status. If there is any exception, it exposes a DOWN status.
In the preceding code, a todo.path property is required. Let’s create a ToDoProperties class that holds the information about this property. You already know about it (see Listing 10-6) .
package com.apress.todo.config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Data
@ConfigurationProperties(prefix = "todo")
public class ToDoProperties {
    private String path;
}
Listing 10-6

com.apress.todo.config.ToDoProperties.java
As you can see, it’s very simple. If you remember, to use a @ConfigurationProperties marked class, it is necessary to call it with @EnableConfigurationProperties. Let’s create the ToDoConfig class to support it (see Listing 10-7).
package com.apress.todo.config;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@EnableConfigurationProperties(ToDoProperties.class)
@Configuration
public class ToDoConfig {
}
Listing 10-7

com.apress.todo.config.ToDoConfig.java
There is nothing extraordinary in this class. Add the application.properties file in the new property.
todo.path=/tmp/todo
If you are using Windows, you can play around with something like
todo.path=C:\\tmp\\todo
Review the documentation for the right characters. So, you are all set. If you re-run your ToDo app, and take a look at the response of the /actuator/health you should get the following.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig27_HTML.jpg
You have the toDoHealthCheck key in the JSON response; and it match with the logic set. Next, fix the issue by making a writeable /tmp/todo directory and if you refresh the page you get.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig28_HTML.jpg
You can configure the status/severity order (e.g., a logging level) by using the following property in your application.properties file .
management.health.status.order=FATAL, DOWN, OUT_OF_SERVICE, UNKNOWN, UP
If you are using the health endpoint over HTTP, every status/severity has its own HTTP code or mapping available.

    DOWN – 503

    OUT_OF_SERVICE – 503

    UP – 200

    DOWN – 200

You can use you own code by using
management.health.status.http-mapping.FATAL=503

Also, you can create your own status, like IN_BAD_CONDITION, by using Health.status("IN_BAD_CONDITION").build();

Creating a custom health indicator with Spring Boot Actuator is very easy!
Spring Boot Actuator Metrics

Nowadays, every system is required to be monitored. It is necessary to keep visibility by watching what is happening in every application, both individually and as a whole. Spring Boot Actuator offers basic metrics and integration and auto-configuration with Micrometer ( http://micrometer.io ).

Micrometer provides a simple face over instrumentation clients for many popular monitoring systems; in other words, you can write the monitoring code once, and use any other third-party system, such as Prometheus, Netflix Atlas, CloudWatch, Datadog, Graphite, Ganglia, JMX, InfluxDB/Telegraf, New Relic, StatsD, SignalFX, and WaveFront (and more coming).

Remember that Spring Boot Actuator has /actuator/metrics. If your run the ToDo app, you get the basic metrics; you learned that in previous sections. What I haven’t shown you is how to create your custom metrics using Micrometer. The idea is to write the code once and use any other third-party monitoring tool. Spring Boot Actuator and Micrometer expose those metrics to the chosen monitoring tool.

Let’s jump directly to implementing Micrometer code and use Prometheus and Grafana to see how easy it is to use.
ToDo App with Micrometer: Prometheus and Grafana

Let’s implement Micrometer code and use Prometheus and Grafana.

So far we have seen that the Spring Data REST module creates web MVC controllers (REST endpoints) on our behalf once it sees all the interfaces that extend the Repository<T,ID> interface. Imagine for a moment that we need to intercept those web requests and start creating an aggregate; a metric that tells us how many times a particular REST endpoint and HTTP method have been requested. This helps us to identify which endpoint is a candidate for a microservice. This interceptor also gets all the requests, including the /actuator ones.
To do this, Spring MVC offers a HandlerInterceptor interface that we can use. It has three default methods, but we only need one of them. Let’s start by creating the ToDoMetricInterceptor class (see Listing 10-8).
package com.apress.todo.interceptor;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class ToDoMetricInterceptor implements HandlerInterceptor {
    private static Logger log = LoggerFactory.getLogger(ToDoMetricInterceptor.class);
    private MeterRegistry registry;
    private String URI, pathKey, METHOD;
    public ToDoMetricInterceptor(MeterRegistry registry) {
        this.registry = registry;
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        URI = request.getRequestURI();
        METHOD = request.getMethod();
        if (!URI.contains("prometheus")){
            log.info(" >> PATH: {}",URI);
            log.info(" >> METHOD: {}", METHOD);
            pathKey = "api_".concat(METHOD.toLowerCase()).concat(URI.replaceAll("/","_").toLowerCase());
            this.registry.counter(pathKey).increment();
        }
    }
}
Listing 10-8

com.apress.todo.interceptor.ToDoMetricInterceptor.java

Listing 10-8 shows the ToDoMetricInterceptor class (it is implementing the HandlerInterceptor interface). This interface has three default methods: preHandle, postHandle, and afterCompletion. This class only implements the afterCompletion method. This method has HttpServletRequest, which is helpful for discovering which endpoint and HTTP method have been requested.

You class is using the MeterRegistry instance , which is part of the Micrometer Framework. The implementation gets the path and the method from the request instance and uses the counter method to get incremented. The pathKey is very simple; if there is a GET request to the /toDos endpoint, the pathKey is api_get_todos. If there is a POST request to the /toDos endpoint, the pathKey is api_post_todos, and so forth. So, if there are several requests to the /toDos, the registry increments (using that pathKey) and aggregates to the existing value.
Next, let’s make sure that ToDoMetricInterceptor is being picked up and configured by Spring MVC. Open the ToDoConfig class and add a MappedInterceptor bean (see Listing 10-9 for version 2).
package com.apress.todo.config;
import com.apress.todo.interceptor.ToDoMetricInterceptor;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;
@EnableConfigurationProperties(ToDoProperties.class)
@Configuration
public class ToDoConfig {
    @Bean
    public MappedInterceptor metricInterceptor(MeterRegistry registry) {
       return new MappedInterceptor(new String[]{"/**"},
                        new ToDoMetricInterceptor(registry));
    }
}
Listing 10-9

com.apress.todo.config.ToDoConfig.java v2

Listing 10-9 shows the new ToDoConfig class, which has MappedInterceptor. It uses ToDoMetricInterceptor for every request by using the "/**" matcher.
Next, let’s add two dependencies that export data to JMX and Prometheus. If you have Maven, you can add the following dependencies to the pom.xml file.
<dependency>
    <groupId>io.micrometer</groupId>
    <artifactId>micrometer-registry-jmx</artifactId>
</dependency>
<dependency>
    <groupId>io.micrometer</groupId>
    <artifactId>micrometer-registry-prometheus</artifactId>
</dependency>
If you are using Gradle, you can add the following dependencies to build.gradle.
compile('io.micrometer:micrometer-registry-jmx')
compile('io.micrometer:micrometer-registry-prometheus')

Spring Boot Actuator auto-configures and registers every Micrometer registry, and in this case, JMX and Prometheus. For Prometheus, the actuator configures the /actuator/prometheus endpoint.
Prerequisites: Using Docker
Before testing the ToDo app with the metrics, it is necessary to install Docker (try to install the latest release).

    Install Docker CE (Community Edition) from https://docs.docker.com/install/

    Install Docker Compose from https://docs.docker.com/compose/install/ 

Why did I choose Docker? Well, it is an easy way to install what we need. And we are going to use it again in the following chapters. Docker Compose facilitates installing Prometheus and Grafana by using Docker’s internal network that allows us to use DNS names.
docker-compose.yml
This is the docker-compose.yml file used to start up Prometheus and Grafana.
version: '3.1'
networks:
  micrometer:
services:
  prometheus:
    image: prom/prometheus
    volumes:
      - ./prometheus/:/etc/prometheus/
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/usr/share/prometheus/console_libraries'
      - '--web.console.templates=/usr/share/prometheus/consoles'
    ports:
      - 9090:9090
    networks:
      - micrometer
    restart: always
  grafana:
    image: grafana/grafana
    user: "104"
    depends_on:
      - prometheus
    volumes:
      - ./grafana/:/etc/grafana/
    ports:
      - 3000:3000
    networks:
      - micrometer
    restart: always

You can create this file with any editor. Remember, it is a YML file and there are no tab spaces for indentation. You need to create two folders: prometheus and grafana. In each folder, there is a file.
In the prometheus folder, there is a prometheus.yml file with the following content.
global:
  scrape_interval:     5s
  evaluation_interval: 5s
scrape_configs:
  - job_name: 'todo-app'
    metrics_path: '/actuator/prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['host.docker.internal:8080']

The important thing about this file is the metrics_path and targets keys. When it finds out about micrometer-registry-prometheus, Spring Boot Actuator auto-configures the /actuator/prometheus endpoint. This value is necessary for metrics_path. Another very important value is the targets key. Prometheus scrapes the /actuator/prometheus endpoint every 5 seconds. It needs to know where it is located (it is using the host.docker.internal domain name). This is the part of Docker that looks for its host (the localhost:8080/todo-actuator app that is running).
The grafana folder contains an empty grafana.ini file . To make sure Grafana takes the default values, you show have the following directory structure.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig29_HTML.jpg
Running the ToDo App Metrics

Now, it’s time to start testing and configuring Grafana to see useful metrics. Run your ToDo app. Check out the logs and make sure that the /actuator/prometheus endpoint is there.
Open a terminal, go where you have the docker-compose.yml file, and execute the following command.
$ docker-compose up
This command line starts the docker-compose engine, and it downloads the images and runs them. Let’s make sure that the Prometheus app works by opening a browser and hitting http://localhost:9090/targets.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig30_HTML.jpg

This means that the prometheus.yml configuration was successfully taken. In other words, Prometheus is scraping the http://localhost:8080/actuator/prometheus endpoint.
Next, let’s configure Grafana.

    1.

    Open another browser tab and hit http://localhost:3000.
     

../images/340891_2_En_10_Chapter/340891_2_En_10_Fig31_HTML.jpg
You can use admin/admin as credentials.

    2.
    You can press the Skip button the following screen.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig32_HTML.jpg
     
    3.
    Click the Add Data Source icon.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig33_HTML.jpg
     
    4.
    Fill out all the required fields. The important ones are

        Name: todo-app

        Type: Prometheus

        URL: http://prometheus:9090

        Scrape interval: 3s
     
    5.
    Click the Save button.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig34_HTML.jpg

    The URL value, http://prometheus:9090, refers to the docker-compose service, which is the internal DNS that docker-compose offers, so no need to do local host. You can leave the other values by default and click Save & Test. If everything works as expected, you see a green banner saying, Data source is working at the bottom of the page.
     

    6.
    You can go home by going back or pointing the browser to http://localhost:3000. You can click the New Dashboard button on the homepage.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig35_HTML.jpg
     
    7.
    You can click the Graph icon, a panel appears. Click Panel Title and then click Edit.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig36_HTML.jpg
     
    8.
    Configure two queries, the api_get_total and api_post_todos_total, which were generated as metrics by the Micrometer and Spring Boot Actuator for Prometheus engine.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig37_HTML.jpg
     
    9.
    Perform requests to the /toDos (several times) and post to the /toDos endpoints. You see something like the next figure.
    ../images/340891_2_En_10_Chapter/340891_2_En_10_Fig38_HTML.jpg
     

Congratulations! You have created custom metrics using Micrometer, Prometheus, and Grafana.
General Stats for Spring Boot with Grafana

I found a very useful configuration for Grafana that allows you to take advantage of every metric that Spring Boot Actuator exposes. This configuration can be imported into Grafana.

Download this configuration from https://grafana.com/dashboards/6756 . The file name is spring-boot-statistics_rev2.json. You need it next.
In the left corner of the Grafana homepage (http://localhost:3000), click the Grafana icon, which opens a side bar. Click the + symbol and choose Import.
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig39_HTML.jpg
Level the default value, but in the Prometheus field, choose todo-app (the data source that you configured earlier).
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig40_HTML.jpg
Click Import—and voilà! You have a complete dashboard with all the Spring Boot Actuator metrics and monitoring!
../images/340891_2_En_10_Chapter/340891_2_En_10_Fig41_HTML.jpg

Take a moment to review every single graph. All the data comes from the /actuator/prometheus endpoint.
You can shut down docker-compose by executing the following in another terminal window.
$ docker-compose down
Note

You can find the solution to this section in the book’s source code on the Apress website or on GitHub at https://github.com/Apress/pro-spring-boot-2 , or on my personal repository at https://github.com/felipeg48/pro-spring-boot-2nd .
Summary

This chapter discussed Spring Boot Actuator, including its endpoints and how customizable it is. With the Actuator module, you can monitor your Spring Boot application, from using the /health endpoint to using /httptrace for more granular debugging.

You learned that you can use Micrometer and plug in any third-party tool to use the Spring Boot actuator metrics.

In the next chapter, you take a step forward and see how to use Spring Integration and Spring Cloud Stream.